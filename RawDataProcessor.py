# -*- coding: utf-8 -*-
"""
@ Omer Luria
Fluidic Technologies Laboratory
Technion - Israel Institute of Technology
luriomer@gmail.com , lomer@technion.ac.il

This script is used to load the raw data file (.csv LabView output) and preprocess it to convert it to a binary
pickled file that can be later opened by the DataAnalyzer code.
The heavy lifting is done here, once, frame by frame.

To properly convert wavefront data to MUT information, the WF_RoC_to_MUT_f_d function has to be defined here in accordance with the optical setup used.
In addition, all the other neccassary parameters (wavelength, measurement accuracy, sensor size, IMU data filter integration time) are also
defined here.
"""
import numpy as np
import pickle
import os.path
import ctypes
import time
import plyer
from DataHandler import DataHandler
from WF_to_MUT import RoC_WF_to_f_d_MUT
import Misc

#%% 
load_filepath = plyer.filechooser.open_file(title="Choose a raw data file to load",preview=True,filters=["*csv","*tsv","*txt"])[0]
print("Selected "+load_filepath)

pending_savefile_selection = True
while pending_savefile_selection:
    save_filepath = plyer.filechooser.save_file(title="Choose a filename to save the processed data",filters=["*pkl"])[0]
    if save_filepath[-4:] not in [".pkl",".PKL"]:
        save_filepath+=".pkl"
        
    if os.path.exists(save_filepath):
        overwrite_question = ctypes.windll.user32.MessageBoxW(0, save_filepath.split('\\')[-1]+" exists.\nWould you like to overwrite it?", "Confirm overwrite", Misc.MB_YESNO | Misc.ICON_QUESTION)
        if overwrite_question == 6:
            pending_savefile_selection = False
    else:
        pending_savefile_selection = False

t1 = time.time()
#%% Specific data processing parameters - change as neccessary
ExpData = DataHandler("Exp Data",filepath=load_filepath,x_mm=11.26,y_mm=11.26,x_pix=73,y_pix=73,lmd=0.590,WF_unc=0.3+0.633/30, 
                      WF_to_MUT=RoC_WF_to_f_d_MUT, MaxZerOrder=36,AccIntExtMaxDiff=0.2,PressExtMaxPa=200e3,ActivePix_min=200,fmin=-np.inf,fmax=np.inf,
                      PupilXCenterOverride = 0.0,PupilYCenterOverride = 0.0,PupilDiameterOverride = 11.26*np.sqrt(2))

ExpData.parse_all()
ExpData.process_all()
#%%
t2 = time.time()
ExpData.logprint("\n'"+ExpData.filename+"' processed successfully in "+"{:.3f}".format(t2-t1)+" seconds.")

ExpData.processed_filepath = save_filepath
ExpData.logprint("\nPickeling to '"+ExpData.processed_filepath+"'...")
processed_file = open(ExpData.processed_filepath,"wb")
pickle.dump(ExpData,processed_file)
processed_file.close()
print("Pickle operation completed successfully.")