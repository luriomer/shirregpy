# -*- coding: utf-8 -*-
"""
@ Omer Luria
Fluidic Technologies Laboratory
Technion - Israel Institute of Technology
luriomer@gmail.com , lomer@technion.ac.il

This code is used to reconstruct wavefront data into a surface, and provide additional related functions.
"""

import numpy as np
import scipy as sp
from scipy.special import jn_zeros
from scipy.fftpack import fftshift, ifftshift, fft2, fftfreq
from scipy.interpolate import interp1d, griddata
import warnings

from ModelFunctions import model_paraboloid, fit_sphere, fit_paraboloid, calc_LiquidDynamics_EigenVals, calc_LiquidDynamics_EigenVals, calc_LiquidDynamics_EigenFuncs, fit_LiquidDynamics_EigenFuncs
from Misc import get_zenith_angles, get_lateral_displacements


def custom_formatwarning(msg, *args, **kwargs):
    # ignore everything except the message
    return str(msg) + '\n'
warnings.formatwarning = custom_formatwarning


class Reconstructor:
    ''' A class for reconstructing surfaces and response functions from wavefront data. '''
    def __init__(self,name,x,y,x_mm,y_mm,WFE,
                 d,f,f_unc,f_eps,delta_beta_eps,lmd,WFE_unc,
                 ImgPix,rigorous_response_analysis,
                 liquid_d0, liquid_nu, liquid_rho, liquid_gamma, liquid_h0,liquid_Bo,
                 EigenOrder_r,EigenOrder_theta,EigenVals_SearchRange):
        ''' Initialize the instance '''
        print("Reconstructor: initializaing instance")
        self.name = name
        self.x = x # x grid, centered on the wavefront center (reference sphere) [mm]
        self.y = y # y grid, centered on the wavefront center (reference sphere) [mm]
        self.x_mm = x_mm # Reconstruction frame width [mm]
        self.y_mm = y_mm # Reconstruction frame height [mm]
        self.WFE = WFE # The wavefront error [um]
        self.d = d # Aperture diameter [mm]
        self.f = f # Focal length [mm]
        self.f_unc = f_unc # Uncertaintly in focal length [mm]
        self.f_eps = f_eps # Max allowed numerical convergence error for the reconstructed focal length [%]
        self.delta_beta_eps = delta_beta_eps # Max allowed numerical convergence error for the reconstructed zenith angles [rad]
        self.lmd = lmd # Working wavelength [um]
        self.WFE_unc = WFE_unc # Typical wavefront error accuracy [um]
        self.ImgPix = ImgPix # Image plane sampling for PSF and MTF simulations [pix]. If it does not divide by 4, we take the closest.
        self.rigorous_response_analysis = rigorous_response_analysis # Whether to calc impulse response or not
        #self.MaxZerOrder = MaxZerOrder # Max Zernike order (up to 36)
        #self.Zc = Zc # Zernike coefficients, array of floats [um]
        self.liquid_d0 = liquid_d0 # Pinning diameter [mm]
        self.liquid_nu = liquid_nu # Liquid kinematic viscosity [cSt]
        self.liquid_rho = liquid_rho # Liquid density [kg/m**3]
        self.liquid_gamma = liquid_gamma # Liquid surface tension [N/m]
        self.liquid_h0 = liquid_h0 # Liquid film thickness [mm]
        self.liquid_Bo = liquid_Bo # Bond number
        self.EigenOrder_r = EigenOrder_r # The order of radial eigenfunctions / number of radial eigenvalues
        self.EigenOrder_theta = EigenOrder_theta # The order of azimuthal eigenfunctions / number of azimuthal eigenvalues
        self.EigenVals_SearchRange = EigenVals_SearchRange # Search range for eigenvalues. (start,stop,step) passed internally to np.arange
        
        if self.ImgPix % 4 != 0:
            self.ImgPix = (ImgPix+4) - ((ImgPix+4) % 4)
            warnings.warn("Warning: image plane sampling pix must be divisable by 4. Taking the closest number of "+str(self.ImgPix)+".\n")
        
        self.EigenVals = calc_LiquidDynamics_EigenVals(EigenOrder_r=self.EigenOrder_r,EigenOrder_theta=self.EigenOrder_theta,Bo=self.liquid_Bo,search_range=self.EigenVals_SearchRange)
        x_full = np.linspace(-1,1,100)
        y_full = np.linspace(-1,1,100)
        X_full, Y_full = np.meshgrid(x_full,y_full)
        r_full = np.hypot(X_full,Y_full)
        theta_full = np.arctan2(Y_full,X_full)
        r_full = np.ma.MaskedArray(r_full,mask=False)
        r_full.mask[r_full>1] = True
        theta_full = np.ma.MaskedArray(theta_full,mask=r_full.mask)
        self.EigenFuncs_full = calc_LiquidDynamics_EigenFuncs(r=r_full,theta=theta_full,EigenVals=self.EigenVals,Bo=self.liquid_Bo)
        
        self.liquid_tau = 3*(self.liquid_nu*1e-6*self.liquid_rho)*(self.liquid_d0*1e-3/2)**4 / (16*(self.liquid_h0*1e-3)**3 *self.liquid_gamma) # Time scale [s]
        self.EigenValTimeScale = self.liquid_tau / self.EigenVals**4
        
    def calc_parameters(self,print_msgs=True):
        ''' (re)Calculates the initial parameters that may have changed when the input is different.
            Separated from init to repeat just this one without resetting everything. '''
        if print_msgs:
            print("\nReconstructor: calculating parameters")
        self.r = np.hypot(self.x,self.y)
        self.theta = np.arctan2(self.y,self.x)
        
        self.dx = np.gradient(self.x,axis=1)
        self.dy = np.gradient(self.y,axis=0)
        
        self.WFE_P2V = np.max(self.WFE)-np.min(self.WFE)
        self.WFE_RMS = np.std(self.WFE)

        self.EigenFuncs = calc_LiquidDynamics_EigenFuncs(r=self.r/(self.liquid_d0/2),theta=self.theta,EigenVals=self.EigenVals,Bo=self.liquid_Bo)
        
        
    def update_coordinate_masks(self,mask):
        ''' Updates masks for x,y,r,theta and EigenFuncs to match an input mask. '''
        # We first make sure we always mask points which are outside of the mechanical aperture of the mirror
        mask[self.r>self.liquid_d0/2] = True
        self.x.mask = mask
        self.y.mask = mask
        self.r.mask = mask
        self.theta.mask = mask
        for i in range(len(self.EigenFuncs)):
            for j in range(len(self.EigenFuncs[i])):
                self.EigenFuncs[i][j].mask = mask


    def calc_impulse_response(self,print_msgs=True):
        ''' Simulates the PSF and MTF on a virtual image plane '''
        if print_msgs:
            print("Reconstructor: calculating impulse response")
        # We start by resampling the wavefront error. We take compressed versions of the grid and data (yielding 1D version of non-masked values).
        # We then use that with griddata and resample with a new grid, as fine as we want. If the interpolation yields any NaNs, it also returns NaNs.
        # We finally mask all invalid values to get a new mask with only valid data.
        self.points = np.column_stack((self.x.compressed(),self.y.compressed()))
        [self.x_resampled,self.y_resampled] = np.meshgrid(np.linspace(np.min(self.x),np.max(self.x),self.ImgPix//2),
                                                np.linspace(np.min(self.y),np.max(self.y),self.ImgPix//2))
        self.WFE_resampled = griddata(self.points,self.WFE.compressed(),(self.x_resampled,self.y_resampled),method='linear',fill_value=np.nan)
        
        # We pad with NaNs to increase the frequency resoltuion without sacrificing the sampling limit 
        self.x_resampled = np.pad(self.x_resampled,(self.ImgPix//4,self.ImgPix//4),'constant',constant_values=np.nan)
        self.x_resampled = np.ma.masked_invalid(self.x_resampled)
        self.y_resampled = np.pad(self.y_resampled,(self.ImgPix//4,self.ImgPix//4),'constant',constant_values=np.nan)
        self.y_resampled = np.ma.masked_invalid(self.y_resampled)
        self.WFE_resampled = np.pad(self.WFE_resampled,(self.ImgPix//4,self.ImgPix//4),'constant',constant_values=np.nan)
        self.WFE_resampled = np.ma.masked_invalid(self.WFE_resampled)
        
        self.AiryDisk_angle = jn_zeros(1,1)[0]/np.pi * self.lmd / (self.d) #Airy disk observation angle [mrad]
        self.liquid_nu_Airy = 1.0/self.AiryDisk_angle # Resolution diffration limit [mrad-1]
        self.liquid_nu_cutoff = self.d/self.lmd           # Max resolution [mrad-1]
        self.liquid_nu = fftshift(fftfreq(self.ImgPix,d=self.d/self.ImgPix)) # Spatial frequency for PSF and MTF [1/mm]

        # Image angular coordinates in [mrad], see Goodman p.77 eq (4-30) after dividing by z=f, and more thoroughly 
        # described in D.G. Voelz, "Computational Fourier Optics (a MATLAB turorial)", page 55
        self.ax_img = self.liquid_nu*self.lmd
        self.liquid_nu_img = fftshift(fftfreq(self.ImgPix,d=np.mean(np.diff(self.ax_img))))# Image angular freqency [mrad-1]
        self.img_pix_scale = (np.max(self.ax_img)-np.min(self.ax_img))/(self.ImgPix) # Image pix scale [mrad/pix]
        self.Nyquist = np.max(self.liquid_nu_img)
        
        #if self.Nyquist < self.liquid_nu_cutoff:
        #    warnings.warn("Warning: the Nyquist frequency is lower than the diffraction-limited cutoff. To resolve diffraction-limited performance, finer detector grid is needed.\n")

        # Propagates the wave to project the point spread function. Assuming far-field (Fraunhofer diffraction).
        # The pupil function is multiplied by the mask inverse since fft ignores the numpy mask - we must put
        # zeros outside of the pupil to get the correct result.
        # We only care about normalized PSF (optical flux is normalized to 1) so we omit all the constant coefficients
        # that appear to multiply everything, and we also noramzlie the result.
        PSF = ifftshift(fft2(fftshift(np.invert(self.WFE_resampled.mask)*np.exp(1j*self.WFE_resampled))))
        PSF = (np.abs(PSF))**2
        self.PSF = PSF/np.max(PSF) # Normalization

        #OTF from the PSF is again a Fourier transform - this time by definition
        OTF = fft2(ifftshift(self.PSF)) # Move the central frequency to the corner
        OTF_max = float(np.real(OTF[0,0]))
        self.OTF = OTF/OTF_max # Normalize by the central frequency signal

        #MTF is the modulus of the OTF
        self.MTF = np.abs(self.OTF)
        
    
    def calc_surf_inner_core(self):
        ''' A method which builds the surface out of a paraboloid and the wavefront error, based on equations (9)-(10) in the manuscript. '''
        # input f is in mm, so in this block all its eppearances are multiplied by 1e3 to calculate in um
        self.parab_ref_surf = model_paraboloid([self.f*1e3], {"r":self.r*1e3})
        self.beta_ref = get_zenith_angles(z = self.parab_ref_surf,
                                         dx = 1e3*self.dx,
                                         dy = 1e3*self.dy)
        # Gradient operation changes the mask (takes out points that cannot be calculated) so we need to update
        self.update_coordinate_masks(self.beta_ref.mask)
        self.parab_ref_surf.mask = self.beta_ref.mask
        self.delta_beta.mask = self.beta_ref.mask
        
        self.z_H = model_paraboloid([self.f*1e3],{"r":(self.liquid_d0/2)*1e3})
        P = 1 + 1 / np.cos(2 * (self.beta_ref + self.delta_beta))
        P_ref = 1 + 1 / np.cos(2 * self.beta_ref)
        self.z_surf = self.WFE/P + self.parab_ref_surf*(P_ref/P) + self.z_H*(P-P_ref)
        # After finding the surface we subtract z_H so the X-Y plane will be the pinning edge (conj mirror plane)
        self.z_surf -= self.z_H

    def calc_surf_outer_core(self,delta_beta):
        ''' A method which takes an array of delta_beta and runs the surface reconstruction. Once reconstructed,
            delta_beta is calculated again and returned - to be used with a fixed point intertive method. '''
        self.delta_beta = delta_beta
        self.calc_surf_inner_core()
        self.beta_surf = get_zenith_angles(z = self.z_surf,
                                          dx = 1e3*self.dx,
                                          dy = 1e3*self.dy)
        
        self.update_coordinate_masks(self.beta_surf.mask | self.beta_ref.mask)
        self.parab_ref_surf.mask = self.beta_surf.mask | self.beta_ref.mask
        self.z_surf.mask = self.beta_surf.mask | self.beta_ref.mask
        
        self.delta_beta = self.beta_surf - self.beta_ref
        
        return self.delta_beta
        

    def calc_surf(self,print_msgs=True):
        ''' Reconstructs the surface and estimates the surface irregularity, as the deviation from a sphere. 
            It's only the irregurity and not power error since we take the best sphere and not a specific one.
            Since the surface errors can be quite large, significant enough to affect to focal length, we can't simply
            assume that our reference parabola matches the nominal focal length based on the wavefront - we must find
            the best reference parabola that yields a final surface with the measured focal length. We do this by 
            optimizing the paraboloid fit for the best reference focal length. '''
        if print_msgs:
            print("Running surface reconstruction")
        # We initially set delta_beta to be zero
        self.delta_beta = np.ma.MaskedArray(np.zeros_like(self.x))
        # Unfortunately scipy.optimize does not support masked arrays so we perform the iteration manually using a while loop
        iteration_diff = 1
        iter_i = 1
        while iteration_diff > self.delta_beta_eps:
            if iter_i > 1e3:
                raise ValueError("\nIteration "+str(iter_i)+": did not converge to delta_beta residual below"+str(self.delta_beta_eps)+" within 1000 iterations.")
            with np.errstate(invalid="ignore"):
                iteration_diff = np.sqrt(np.sum((self.delta_beta-self.calc_surf_outer_core(self.delta_beta))**2)/np.ma.count(self.delta_beta))
            #("Iteration "+str(iter_i)+": delta_beta residual "+"{:0.4e}".format(iteration_diff))
            iter_i += 1
        
        self.calc_surf_outer_core(self.delta_beta)
        
        self.delta_x , self.delta_y = get_lateral_displacements(z = self.z_surf/1e3,
                                                                z_H = self.z_H/1e3,
                                                                dx = self.dx,
                                                                dy = self.dy,
                                                                beta = self.beta_surf)
        
        # Make sure the lateral displacements are less than the pixel resolution, so no overlap would be possible.
        if np.max(self.delta_x) >= 0.25 * np.min(self.dx) or np.max(self.delta_y) >= 0.25 * np.min(self.dy):
            warnings.warn("\nIteration "+str(iter_i)+": excessive lateral displacements were observed, reconstructed data points may overlap!")
        
        self.parab_ref_surf_unc = self.f_unc*1e3 * (self.r/self.f)**2 / 4
        self.z_surf_unc = np.hypot(self.parab_ref_surf_unc,self.WFE_unc)
        self.z_surf_unc_rms = np.std(self.z_surf_unc)
        
        self.z_surf_sphere_fit_res = fit_sphere(x = self.x*1e3,
                                              y = self.y*1e3,
                                              z = self.z_surf,
                                              z_unc=self.z_surf_unc)
        
        # Extract the surface coordinates from the fit result. Convert X and Y back to mm
        # We also get back Z, and note that it will return masked regardless of how it was entered
        self.x_surf_flat = self.z_surf_sphere_fit_res["x_flat"]/1e3
        self.y_surf_flat = self.z_surf_sphere_fit_res["y_flat"]/1e3
        self.z_surf_flat = self.z_surf_sphere_fit_res["z_flat"]
        self.surf_sphere_fit_RoC = self.z_surf_sphere_fit_res["RoC"]/1e3
        self.surf_sphere_fit_RoC_unc = self.z_surf_sphere_fit_res["RoC_unc_corr"]/1e3
        self.x0_surf_sphere_fit = self.z_surf_sphere_fit_res["x0"]/1e3
        self.y0_surf_sphere_fit = self.z_surf_sphere_fit_res["y0"]/1e3
        self.z0_surf_sphere_fit = self.z_surf_sphere_fit_res["z0"]
        self.R_sq_surf_sphere_fit = self.z_surf_sphere_fit_res["R_sq"]
        self.R_sq_adj_surf_sphere_fit = self.z_surf_sphere_fit_res["R_sq_adj"]
        self.surf_sphere_fit_z = self.z_surf_sphere_fit_res["z_sph"]
        self.surf_sphere_fit_resid = self.z_surf_sphere_fit_res["z_diff"]
        self.surf_sphere_fit_resid_rms = self.z_surf_sphere_fit_res["z_diff_rms"]
        
        sphere_fit_resid_diff = self.surf_sphere_fit_resid - np.mean(self.surf_sphere_fit_resid)
        self.surf_sphere_fit_resid_rms_unc = np.sum(sphere_fit_resid_diff*self.z_surf_unc)/np.sqrt(np.sum(sphere_fit_resid_diff**2))/np.sqrt(np.ma.count(sphere_fit_resid_diff))
        
        # Decompose the surface irregularity map (deviation from a sphere) to the eigenfunctions which describe
        # the dynamics of thin liquid films.
        self.surf_EigenFuncs_fit = fit_LiquidDynamics_EigenFuncs(self.z_surf,self.EigenFuncs)
        self.surf_EigenFuncs_fit_AnBn = self.surf_EigenFuncs_fit["AnBn"]
        self.surf_EigenFuncs_fit_z = np.zeros_like(self.z_surf)
        for i in range(len(self.EigenFuncs)):
            for j in range(len(self.EigenFuncs[i])):
                if ~self.surf_EigenFuncs_fit_AnBn.mask[i,j]:
                    self.surf_EigenFuncs_fit_z += self.EigenFuncs[i][j] * self.surf_EigenFuncs_fit_AnBn[i,j]
        self.surf_EigenFuncs_fit_z_resid = self.z_surf - self.surf_EigenFuncs_fit_z
        self.surf_EigenFuncs_fit_z_resid_rms = np.std(self.surf_EigenFuncs_fit_z_resid)
        
        EigenFuncs_fit_resid_diff = self.surf_EigenFuncs_fit_z_resid - np.mean(self.surf_EigenFuncs_fit_z_resid)
        self.surf_sphere_fit_resid_rms_unc = np.sum(EigenFuncs_fit_resid_diff*self.z_surf_unc)/np.sqrt(np.sum(EigenFuncs_fit_resid_diff**2))/np.sqrt(np.ma.count(EigenFuncs_fit_resid_diff))
        
    def calc_all(self,print_msgs=True):
        self.calc_parameters(print_msgs)
        if self.rigorous_response_analysis:
            self.calc_impulse_response(print_msgs)
        self.calc_surf(print_msgs)
    
        
    def calc_EE(self,rEE_size=100):
        ''' Calculates encircled energy for the PSF. '''
        # We calculate the centroid of the PSF
        self.x_psf = self.x_resampled*self.img_pix_scale
        self.y_psf = self.y_resampled*self.img_pix_scale
        self.x0_psf = np.average(self.x_psf,weights=self.PSF)
        self.y0_psf = np.average(self.y_psf,weights=self.PSF)
        
        
        # The radial coordinate is now shifted so the origin for the EE is the PSF centroid
        self.r_psf = np.hypot((self.x_psf-self.x0_psf),(self.y_psf-self.y0_psf))
        
        # We create an inverted normalized PSF radius coordinate to run in reverse
        self.rEE = np.linspace(np.max(self.r_psf),np.min(self.r_psf),rEE_size)
        self.EE = np.zeros_like(self.rEE)
        self.mask_EE = np.full([self.ImgPix,self.ImgPix],1.0)
        total_EE = np.sum(self.PSF)
        for i,rEEi in enumerate(self.rEE):
            if rEEi > 0:
                self.mask_EE[self.r_psf>rEEi] = 0.0
                self.masked_PSF = np.ma.MaskedArray(self.PSF,mask=1-self.mask_EE)
                self.EE[i] = np.sum(self.masked_PSF)/total_EE
            elif rEEi == 0:
                self.EE[i] = 0
        
        # Interpolating the EE, with the radius denormalized to be [mrad]
        self.rEE_interp = interp1d(self.EE,self.rEE)
        
        
        # def calc_Zernike_WF(self):
        #     ''' Reconstructs the wavefront from Zernike polynomials. MaxZerOrder defines how many Zernike orders to take. '''
        #     # Calculate Zernike polynomials (normalized polynomials, without the coefficients) for the calculation grid
        #     self.Zp = model_Zernike(self.r,self.theta)
        #     if len(self.Zc) < self.MaxZerOrder:
        #         warnings.warn("Warning: the requested Zernike order is higher than the number of coefficients provided. Padding with zeros.\n")
        #         self.Zc = np.pad(self.Zc,(0,self.MaxZerOrder-len(self.Zc)))
        #     if self.MaxZerOrder < len(self.Zc):
        #         warnings.warn("Warning: the requested Zernike order is lower than the number of coefficients provided. Higher orders will be discarded.\n")
            
        #     self.WF_Zernike = np.ma.MaskedArray(np.zeros([self.PupilPix,self.PupilPix]),mask=self.mask_norm)
        #     for i in range(self.MaxZerOrder):
        #         self.WF_Zernike += self.Zp[i] * self.Zc[i]

        #     # Calculate P-V and RMS
        #     self.P2V_Zernike = np.max(self.WF_Zernike)-np.min(self.WF_Zernike)
        #     self.RMS_Zernike = np.std(self.WF_Zernike)