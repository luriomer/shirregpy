# -*- coding: utf-8 -*-
"""
Created on Sat Jun  4 19:07:22 2022

@author: Omer
"""
import numpy as np
import matplotlib.pyplot as plt
from lmfit import Model, Parameters

#%% Nonlinear optimization of tilt+defocus (plane + exact sphere)
def per_iteration(pars, iteration, resid, *args, **kws):
    MSE = np.sqrt(np.sum(resid**2))/len(resid)
    print("Iteration "+str(iteration)+", MSE = "+str(MSE)+"\n",[f"{p.name} = {p.value:.5f}" for p in pars.values()],"\n")

# Define models
def Plane(x,y,a,b):
    ''' A plane that crosses the origin (pure tilt). '''
    return -a*x-b*y

def SphereCap(x,y,x0,y0,z0,R,c):
    ''' A displaced spherical cap. Returns zero outside the cap. '''
    quad = R**2 - (x-x0)**2 - (y-y0)**2
    quad = np.where(quad>=0,quad,0)
    return z0 + c*np.sqrt(quad)

def fit_Tilt_Defocus(xdata,ydata,zdata,params,iter_calls=False,report=False):

    PlaneModel = Model(Plane,independent_vars=["x","y"],param_names=["a","b"])
    SphereCapModel = Model(SphereCap,independent_vars=["x","y"],param_names=["x0","y0","z0","R","c"])
    TiltedSphereCapModel = PlaneModel + SphereCapModel
    
    if iter_calls:
        iter_cb = per_iteration
    else:
        iter_cb = None
    
    res = TiltedSphereCapModel.fit(data=zdata,x=xdata,y=ydata,params=params,method="leastsq",iter_cb=iter_cb)
    if report:
        print(res.fit_report())
    return res

params = Parameters()
#%% Testing
'''
# Create fake data to test
xdata,ydata = np.meshgrid(np.linspace(-1,1,100),np.linspace(-1,1,100))
zdata = SphereCap(xdata,ydata,x0=2.17,y0=5.98,z0=2,R=50,c=1) + Plane(xdata,ydata,a=0.1,b=0.25) #+ 0.2*np.sin((xdata**2+ydata**2)*5) + 0.2*np.sin((xdata**2+ydata**2)*20)

# Fit call
res = fit_Tilt_Defocus(xdata,ydata,zdata,guess={"a":0.0,
                                          "b":0.0,
                                          "x0":0.0,
                                          "y0":0.0,
                                          "z0":0.0,
                                          "R":40,
                                          "c":1},iter_calls=False,report=True)

# Plots
fig = plt.figure()
ax = fig.add_subplot(projection='3d')
scat = ax.scatter(xdata[::2,::2],ydata[::2,::2],zdata[::2,::2],label="Data",color="blue")

#surf_init = ax.plot_surface(xdata,ydata,res.init_fit,label="Initial guess",color="red",alpha=0.7)
#surf_init._facecolors2d = surf_init._facecolor3d
#surf_init._edgecolors2d = surf_init._edgecolor3d

surf_best = ax.plot_surface(xdata,ydata,res.best_fit,label="Best fit",color="green",alpha=0.7)
surf_best._facecolors2d = surf_best._facecolor3d
surf_best._edgecolors2d = surf_best._edgecolor3d

ax.set_xlabel("x")
ax.set_ylabel("y")
ax.set_zlabel("z")
ax.set_box_aspect((np.ptp(xdata), np.ptp(xdata), np.ptp(zdata)))
plt.legend()
'''