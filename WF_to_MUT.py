# -*- coding: utf-8 -*-
"""
@ Omer Luria
Fluidic Technologies Laboratory
Technion - Israel Institute of Technology
luriomer@gmail.com , lomer@technion.ac.il

This code is used to define the wavefront to MUT conversion. The RoC_WF_to_f_d_MUT function has to be defined
in accordance with the optical setup used.
"""

import numpy as np
from scipy.interpolate import CubicSpline

# Get the data
SHIRREG_RoC_WF_to_f_MUT = np.loadtxt("SHIRREG_Steinheil_RoC_WF_to_f_MUT_ZemaxMapping.txt")
RoC_WFs = SHIRREG_RoC_WF_to_f_MUT[:,0]
f_LUTs = SHIRREG_RoC_WF_to_f_MUT[:,1]

# Sort both arrays, so the RoCs will be strictly ascending for the interpolator
f_argindex = np.argsort(RoC_WFs)
RoC_WFs = RoC_WFs[f_argindex]
f_LUTs = f_LUTs[f_argindex]

# Interpolate
SHIRREG_RoC_WF_to_f_MUT_interp = CubicSpline(RoC_WFs,f_LUTs,extrapolate=False)

# We also need the derivative for the uncertainty propagation
dfMUT_dRoC_WF = SHIRREG_RoC_WF_to_f_MUT_interp.derivative()

# For the diameters ~~ This part is currently disabled since it creates artificially magnification differences
# as a function of the chosen pupil, and hence slightly different scales. We instead simply set the magnification
# to be constant, as we extract from a linear fit in the analysis sheet (based on the Zemax mapping).
'''
SHIRREG_d_SHWS_to_d_MUT = np.loadtxt("SHIRREG_Steinheil_d_SHWS_to_d_MUT_ZemaxMapping.txt")
d_SHWS = SHIRREG_d_SHWS_to_d_MUT[:,0]
d_MUT = SHIRREG_d_SHWS_to_d_MUT[:,1]

d_argindex = np.argsort(d_SHWS)
d_SHWS = d_SHWS[d_argindex]
d_MUT = d_MUT[d_argindex]


SHIRREG_d_SHWS_to_d_MUT_interp = CubicSpline(d_SHWS,d_MUT,extrapolate=False)
dd_MUT_dd_SHWS = SHIRREG_d_SHWS_to_d_MUT_interp.derivative()
'''
dd_MUT_dd_SHWS = 1/0.2553

def RoC_WF_to_f_d_MUT(RoC_WF,d_SHWS):
    ''' Define a function that takes RoC_WF and d_SHWSs and returns f_MUT and d_MUT as a tuple.
        Only edit the bounded block. 
        RoC_WF,d_SHWS,f_MUT,d_MUT are all in mm
        dfMUT_dRoC_WF anddd_MUT_dd_SHWS are dimensionless
        The sign of RoC_WF must be consistent with the SHWS convention - 
        position is concave wavefront (diverging beam, focal point in front of the SH)
        negative is convex wavefront (converging beam, focal point behind the SH) '''
#%%    ''' Start of definition '''
    # PF Dec 21
    #f_MUT = 97-RoC_WF
    #d_MUT = np.abs(f_MUT/RoC_WF) * d_SHWS
    
    #SHIRREG with Steinheil triplet
    if (RoC_WF < -5e4 or RoC_WF > 2e4):
        f_MUT = 162.45
    elif (RoC_WF > -10.163 and RoC_WF < -10.143):
        f_MUT = 1e4
    else:
        f_MUT = float(SHIRREG_RoC_WF_to_f_MUT_interp(RoC_WF))
 
    if d_SHWS >= 0.2 and d_SHWS <= 16:
        # We take 16 so we can cover the entire rectangular screen.
        # Currently we disabled this interpolation and set the magnification to be constant.
        d_MUT = d_SHWS/0.2553#float(SHIRREG_d_SHWS_to_d_MUT_interp(d_SHWS))
    else:
        raise ValueError("SHWS beam size is out of range for the SHIRREG setup!")
    ''' End of definition '''
#%%    
    return {"f_MUT":f_MUT, "d_MUT":d_MUT,"dfMUT_dRoC_WF":dfMUT_dRoC_WF(RoC_WF), "dd_MUT_dd_SHWS":dd_MUT_dd_SHWS}