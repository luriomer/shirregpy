# -*- coding: utf-8 -*-
"""
Created on Sat Jun  4 19:07:22 2022

@author: Omer
"""
import numpy as np
from lmfit import Model, Parameters
import matplotlib.pyplot as plt

def per_iteration(pars, iteration, resid, *args, **kws):
    MSE = np.sqrt(np.sum(resid**2))/len(resid)
    print("Iteration "+str(iteration)+", MSE = "+str(MSE)+"\n",[f"{p.name} = {p.value:.5f}" for p in pars.values()],"\n")

# Define models
def Plane(x,y,a,b):
    ''' A plane that crosses the origin (pure tilt). '''
    return -a*x-b*y

def SphereCap(x,y,x0,y0,z0,R,c):
    ''' A displaced spherical cap. Returns zero outside the cap. '''
    quad = R**2 - (x-x0)**2 - (y-y0)**2
    quad = np.where(quad>=0,quad,0)
    return z0 + c*np.sqrt(quad)

PlaneModel = Model(Plane,independent_vars=["x","y"],param_names=["a","b"])
SphereCapModel = Model(SphereCap,independent_vars=["x","y"],param_names=["x0","y0","z0","R","c"])
TiltedSphereCapModel = PlaneModel + SphereCapModel

# Set up parameters
params = Parameters()
params.add("a", value=0.0,  vary=True)
params.add("b", value=0.0,  vary=True)
params.add("x0",value=0.0,  vary=True)
params.add("y0",value=0.0,  vary=True)
params.add("z0",value=0.0, vary=True)
params.add("R", value=25,   vary=True,min=0)
params.add("c", value=+1.0, vary=False)

# Create fake data to test
xdata,ydata = np.meshgrid(np.linspace(-10,10,100),np.linspace(-10,10,100))
zdata = SphereCap(xdata,ydata,x0=2.17,y0=5.98,z0=2,R=50,c=1) + Plane(xdata,ydata,a=0.1,b=0.25) #+ 0.2*np.sin((xdata**2+ydata**2)*5) + 0.2*np.sin((xdata**2+ydata**2)*20)

# Fit call
res = TiltedSphereCapModel.fit(data=zdata,x=xdata,y=ydata,params=params,method="leastsq",iter_cb=per_iteration)
print(res.fit_report())

#%% Plots
fig = plt.figure()
ax = fig.add_subplot(projection='3d')
scat = ax.scatter(xdata[::2,::2],ydata[::2,::2],zdata[::2,::2],label="Data",color="blue")

#surf_init = ax.plot_surface(xdata,ydata,res.init_fit,label="Initial guess",color="red",alpha=0.7)
#surf_init._facecolors2d = surf_init._facecolor3d
#surf_init._edgecolors2d = surf_init._edgecolor3d

surf_best = ax.plot_surface(xdata,ydata,res.best_fit,label="Best fit",color="green",alpha=0.7)
surf_best._facecolors2d = surf_best._facecolor3d
surf_best._edgecolors2d = surf_best._edgecolor3d

ax.set_xlabel("x")
ax.set_ylabel("y")
ax.set_zlabel("z")
ax.set_box_aspect((np.ptp(xdata), np.ptp(xdata), np.ptp(zdata)))
plt.legend()