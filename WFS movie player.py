# -*- coding: utf-8 -*-
"""
Created on Mon Dec 27 17:10:01 2021

@author: Omer
"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import datetime
import plyer
#from tkinter.filedialog import askopenfilename
filepath = plyer.filechooser.open_file()[0]
filename = filepath.split('\\')[-1]
datafile = np.genfromtxt(filename,delimiter=",",skip_header=1,usecols=range(73))
WFs = []
ts = []
Ts = []
ROCs = []
fs = []
PVs = []
RMSs = []

wf_num = int((np.shape(datafile)[0]+1)/75)
for i in range(wf_num):
    WFs.append(datafile[75*i+1:75*i+74,0:73])
    ts.append(datafile[75*i,0])
    Ts.append(str(datetime.timedelta(seconds=ts[i])))
    ROCs.append(datafile[75*i,7])
    fs.append(datafile[75*i,8])
    PVs.append(datafile[75*i,9])
    RMSs.append(datafile[75*i,10])

plt.figure()
ax = plt.gca()
ax.set_aspect(1)

def animate(i):
    plt.cla()
    plt.contourf(WFs[i])
    plt.title(Ts[i]+" , f = "+str(round(fs[i],2))+", RMS = "+str(round(RMSs[i],2))+" um")

ani = FuncAnimation(plt.gcf(),animate,interval=100)

plt.xlabel("Pupil X [a.u.]")
plt.ylabel("Pupil Y [a.u.]")
plt.tight_layout()
plt.show()