# -*- coding: utf-8 -*-
"""
@ Omer Luria
Fluidic Technologies Laboratory
Technion - Israel Institute of Technology
luriomer@gmail.com , lomer@technion.ac.il

This code handles the experimental data itself - parses and processes it frame by frame.
"""

import numpy as np

import matplotlib.pyplot as plt

import sys
import datetime
import time
import ctypes

from ModelFunctions import model_Zernike, fit_Zernike_coefficients, ZerNames, fit_sphere, fit_plane
import Misc

plt.close("all")

class DataHandler:
    ''' A class to handle experimental data saved in Ericson format (LabView Flight Control v2).
        Reads the data from the csv and defined the storage iterables. '''
    def __init__(self,name,filepath,x_mm,y_mm,x_pix,y_pix,lmd,WF_unc,WF_to_MUT,MaxZerOrder,AccIntExtMaxDiff,
                 PressExtMaxPa,PupilXCenterOverride=np.nan,PupilYCenterOverride=np.nan,PupilDiameterOverride=np.nan,
                 ActivePix_min=100,fmin=None,fmax=None):
        self.log = ""
        self.logprint("Creating Data instance")
        self.name = name
        self.x_mm = x_mm # Frame width [mm] (float)
        self.y_mm = y_mm # Frame height [mm] (float)
        self.x_pix = x_pix # Number of pixels in x direction (int)
        self.y_pix = y_pix # Number of pixels in y direction (int)
        self.lmd = lmd     # Wavelength [um] (float)
        self.WF_unc = WF_unc       # Measured wavefront uncertaintly [um]
        self.WF_to_MUT = WF_to_MUT # Relations between wavefront RoC to focal length and aperture of MUT
        self.MaxZerOrder = MaxZerOrder # Max Zernike order to fit (int)
        self.AccIntExtMaxDiff = AccIntExtMaxDiff # A threshold used to filter out spikes in the external IMU, based on the difference from the internal IMU.
        self.PressExtMaxPa = PressExtMaxPa # A threshold used to filter out spikes in the external pressure sensor, by setting a max value.
        self.PupilXCenterOverride = PupilXCenterOverride # Override all frames with a specific pupil center x [mm]
        self.PupilYCenterOverride = PupilYCenterOverride # Override all frames with a specific pupil center y [mm]
        self.PupilDiameterOverride = PupilDiameterOverride           # Override all frames with a specific pupil radius [mm]
        self.fmin = fmin # A threshold for the minimal focal length possible for the MUT
        self.fmax = fmax # A threshold for the maximal focal length possible for the MUT
        
        self.ActivePix_min = ActivePix_min # Min number of active pixels to decompose
        
        self.ZerNames = ZerNames
        
        self.x,self.y = np.meshgrid(np.ma.MaskedArray(np.linspace(-self.x_mm/2,self.x_mm/2,self.x_pix),mask=False),
                                    np.ma.MaskedArray(np.linspace(-self.y_mm/2,self.y_mm/2,self.y_pix),mask=False))
        self.r = np.hypot(self.x,self.y)
        self.theta = np.arctan2(self.y,self.x)
        
        self.x_scale = self.x_mm/self.x_pix # x pix scale [mm/pix]
        self.y_scale = self.y_mm/self.y_pix # y pix scale [mm/pix]
        self.filepath = filepath
        if self.filepath == []:
            sys.exit()
        
        self.filename = self.filepath.split('\\')[-1]
        self.logprint("Loading file '"+self.filepath.split('\\')[-1]+"'...")
        self.datafile = np.genfromtxt(self.filepath,delimiter=",",comments="T")
        self.frame_count = int((np.shape(self.datafile)[0]+1)/74)
        self.WF = [np.ma.MaskedArray(np.zeros((self.x_pix,self.y_pix)),mask=False)]*self.frame_count
        self.WFE = [np.ma.MaskedArray(np.zeros((self.x_pix,self.y_pix)),mask=False)]*self.frame_count
        
        self.FrameActivePix = [None]*self.frame_count
        self.PupilActivePix = [None]*self.frame_count
        
        self.use_pupil_for_mask = [True]*self.frame_count
        self.PupilMask = [np.full([self.x_pix,self.y_pix],False)]*self.frame_count
        
        self.PC_RTC = np.full(self.frame_count,np.nan)

        self.FrameLost = np.full(self.frame_count,np.nan)
        self.WFMeasType = np.full(self.frame_count,np.nan)
        self.WFMeasUnit = np.full(self.frame_count,np.nan)
        
        self.BeamXSize = np.full(self.frame_count,np.nan)
        self.BeamYSize = np.full(self.frame_count,np.nan)
        self.BeamNomSize = np.full(self.frame_count,np.nan)
        self.BeamXCenter = np.full(self.frame_count,np.nan)
        self.BeamYCenter = np.full(self.frame_count,np.nan)
        
        self.PupilXCenter = np.full(self.frame_count,self.PupilXCenterOverride)
        self.PupilYCenter = np.full(self.frame_count,self.PupilYCenterOverride)
        self.PupilDiameter = np.full(self.frame_count,self.PupilDiameterOverride)
        
        self.x0_WF = np.full(self.frame_count,np.nan)
        self.y0_WF = np.full(self.frame_count,np.nan)
        self.RoC_WF = np.full(self.frame_count,np.nan)
        self.RoC_WF_unc = np.full(self.frame_count,np.nan)
        self.f_MUT = np.full(self.frame_count,np.nan)
        self.f_MUT_unc = np.full(self.frame_count,np.nan)
        self.d_MUT = np.full(self.frame_count,np.nan)
        self.d_MUT_unc = np.full(self.frame_count,np.nan)
        self.conj_mag = np.full(self.frame_count,np.nan)
        self.dfMUT_dRoC_WF = [None]*self.frame_count
        self.dd_MUT_dd_SHWS = [None]*self.frame_count

        self.TL_Z_num = [None]*self.frame_count
        
        self.SyncButtonLabView = [None]*self.frame_count
        
        self.SyncButtonIMU = [None]*self.frame_count
        self.LabViewTrigger = [None]*self.frame_count
        
        self.PumpAction = [None]*self.frame_count
        self.PumpMode = [None]*self.frame_count
        self.PumpDirection = [None]*self.frame_count
        self.PumpConnection = [None]*self.frame_count
        
        self.IMUChipTimeMillis = np.full(self.frame_count,np.nan)
        self.IMU_RTC_date = [""]*self.frame_count
        
        self.time = np.full(self.frame_count,np.nan) # The array which holds the accurate time in [s].
        self.time_RTC = [None]*self.frame_count # RTC format version of the time array
        self.time_patched_list = [] # A list of all time frames which were patched.
        
        self.AccInt = np.full((self.frame_count,3),np.nan)
        self.GyroInt = np.full((self.frame_count,3),np.nan)
        self.AccExt = np.full((self.frame_count,3),np.nan)
        self.AccExt_flag = np.full(self.frame_count,np.nan)
        self.GyroExt = np.full((self.frame_count,3),np.nan)
        self.PressExt = np.full(self.frame_count,np.nan)
        
        self.Zc = np.full((self.frame_count,self.MaxZerOrder),np.nan)
        self.ZRsq = np.full(self.frame_count,np.nan)
        self.ZRsq_adj = np.full(self.frame_count,np.nan)
        self.processing_success = [None]*self.frame_count
        self.SurfRoC = np.full(self.frame_count,np.nan,dtype=float)
        self.SurfErrRMS = np.full(self.frame_count,np.nan,dtype=float)
        self.Surf_EigenFuncs_fit_AnBn = [None]*self.frame_count
        
        
    def parse_all(self):
        ''' Parse all the frames by assigning the different variables with data from the datafile '''
        for i in range(self.frame_count):
            sys.stdout.write('\033[2K\033[1G')
            self.logprint("\rParsing frame "+str(i)+"/"+str(self.frame_count-1)+" ("+str(round(100*i/(self.frame_count-1)))+"%)",end="")
            # We flip the frames around the x axis, since the Thorlabs WFS definition for the positive +y is upward while the array
            # standard in Python and MATLAB is positive downwards. It does not really matter, but just to remain consistent.
            self.WF[i] = np.ma.MaskedArray(np.flip((self.datafile[(self.y_pix+1)*i+1:(self.y_pix+1)*(i+1),0:(self.x_pix)]),0),mask=False)
            self.PupilMask[i][np.isnan(self.WF[i])] = True
            self.WF[i].mask = self.PupilMask[i]
            
            self.FrameActivePix[i] = np.count_nonzero(~np.isnan(self.WF[i]))# More bulletproof  than np.count_nonzero(self.PupilMask[i])
            
            self.PC_RTC[i] = self.datafile[(self.y_pix+1)*i,0]
            
            self.FrameLost[i] = self.datafile[(self.y_pix+1)*i,1]
            
            self.WFMeasType[i] = self.datafile[(self.y_pix+1)*i,2]
            if self.WFMeasType[i] != 0:
                raise ValueError("Frame "+str(i)+": Measurement type indicator is not 0 - check Thorlabs WFS settings!")
            self.WFMeasUnit[i] = self.datafile[(self.y_pix+1)*i,3]
            if self.WFMeasUnit[i] != 0:
                raise ValueError("Frame "+str(i)+": Wavefront unit indicator is not 0 - check Thorlabs WFS settings!")
            self.BeamXSize[i] = self.datafile[(self.y_pix+1)*i,4]
            self.BeamYSize[i] = self.datafile[(self.y_pix+1)*i,5]
            self.BeamNomSize[i] = (self.BeamXSize[i]+self.BeamYSize[i])/2
            self.BeamXCenter[i] = self.datafile[(self.y_pix+1)*i,6]
            self.BeamYCenter[i] = self.datafile[(self.y_pix+1)*i,7]
            
            if self.BeamNomSize[i] > np.hypot(self.x_mm,self.y_mm):
                self.BeamNomSize[i] = np.hypot(self.x_mm,self.y_mm)
                self.BeamXSize[i] = 0
                self.BeamYSize[i] = 0
                self.logprint("Frame "+(str(i-1))+": input beam size is larger than physical sensor, saturating to the confining circle of "+"{:.1}".format(np.hypot(self.x_mm,self.y_mm)))
            elif self.BeamNomSize[i] < 0.2 :
                self.BeamNomSize[i] = 0.2
                self.BeamXSize[i] = 0
                self.BeamYSize[i] = 0
                self.logprint("Frame "+str(i-1)+": input beam size is smaller than the threshold, saturating to 0.2 mm.")
            
            # We define the initial pupils according to the values give by the Thorlabs WFS, unless the user defined overrides
            if np.isnan(self.PupilXCenterOverride):
                self.PupilXCenter[i] = self.BeamXCenter[i]
            if np.isnan(self.PupilYCenterOverride):
                self.PupilYCenter[i] = self.BeamYCenter[i]
            if np.isnan(self.PupilDiameterOverride):
                self.PupilDiameter[i] = self.BeamNomSize[i]
            
            self.TL_Z_num[i] = int(self.datafile[(self.y_pix+1)*i,13])
            self.SyncButtonLabView[i] = int(self.datafile[(self.y_pix+1)*i,(14+self.TL_Z_num[i])])
            self.PumpAction[i] = int(self.datafile[(self.y_pix+1)*i,(15+self.TL_Z_num[i])])
            self.PumpMode[i] = int(self.datafile[(self.y_pix+1)*i,(16+self.TL_Z_num[i])])
            self.PumpDirection[i] = int(self.datafile[(self.y_pix+1)*i,(17+self.TL_Z_num[i])])
            self.PumpConnection[i] = int(self.datafile[(self.y_pix+1)*i,(18+self.TL_Z_num[i])])
            
            self.SyncButtonIMU[i] = int(self.datafile[(self.y_pix+1)*i,(19+self.TL_Z_num[i])])
            self.LabViewTrigger[i] = int(self.datafile[(self.y_pix+1)*i,(20+self.TL_Z_num[i])])
            
            
            self.IMUChipTimeMillis[i] = int(self.datafile[(self.y_pix+1)*i,(21+self.TL_Z_num[i])])
            self.IMU_RTC_date[i] = str(self.datafile[(self.y_pix+1)*i,(22+self.TL_Z_num[i])])[0:7]
            
            # The first time frame was set in init as the initial PC_RTC. After that, we prefer to take the differences from the IMU millis timer.
            # However, it is less reliable so in time frames where the IMU resets the value, we locally patch with the difference from the PC_RTC.
            if i == 0:
                self.time[i] = self.PC_RTC[i]
            elif i>0:
                # We make sure that all the data was at the same day
                if self.IMU_RTC_date[i] != self.IMU_RTC_date[i-1]:
                    msg = ctypes.windll.user32.MessageBoxW(0,"Inconsistent values were detected in the IMU RTC date. This can happen if the clock crossed midnight, or if the IMU was disconnected.\nWould you like to continue processing?", Misc.ICON_INFO,4)
                    if msg == 7:
                        raise ValueError("Inconsistent values were detected in the IMU RTC date. Processing aborted by the user.")
                # We make sure that the data is provided in an ascending order by looking at the PC RTC
                if self.PC_RTC[i] < self.PC_RTC[i-1]:
                    raise ValueError("Inconsistent values were detected in the PC RTC time. This can happen if the clock crossed midnight, or if the raw data files were wrongly mixed. Please check your data.")
                
                if self.IMUChipTimeMillis[i] <= self.IMUChipTimeMillis[i-1]:
                    self.logprint("\nFrame "+(str(i-1))+": IMU timer has either reset or stopped. Patching using PC_RTC data.")
                    self.time_patched_list.append(i-1)
                    self.time[i] = self.time[i-1] + self.PC_RTC[i]-self.PC_RTC[i-1]
                else:
                    self.time[i] = self.time[i-1] + (self.IMUChipTimeMillis[i] - self.IMUChipTimeMillis[i-1])/1e3
                
                
            self.time_RTC[i] = str(datetime.timedelta(seconds=round(self.time[i],2)))
            self.AccInt[i] = self.datafile[(self.y_pix+1)*i,(23+self.TL_Z_num[i]):(26+self.TL_Z_num[i])]
            
            self.GyroInt[i] = self.datafile[(self.y_pix+1)*i,(26+self.TL_Z_num[i]):(29+self.TL_Z_num[i])]
            self.AccExt[i] = self.datafile[(self.y_pix+1)*i,(29+self.TL_Z_num[i]):(32+self.TL_Z_num[i])]
            
            self.GyroExt[i] = self.datafile[(self.y_pix+1)*i,(32+self.TL_Z_num[i]):(35+self.TL_Z_num[i])]
            self.PressExt[i] = int(self.datafile[(self.y_pix+1)*i,(41+self.TL_Z_num[i])])

        self.AccInt_res = np.linalg.norm(self.AccInt,axis=1)
        self.AccExt_res = np.linalg.norm(self.AccExt,axis=1)
        self.AccExt_res_filt = np.copy(self.AccExt_res)
        self.AccExt_res_filt[np.abs(self.AccExt_res_filt-self.AccInt_res)>self.AccIntExtMaxDiff] = np.nan
        self.AccExt_res_filt = Misc.interpolate_nans(self.AccExt_res_filt)
        
        self.PressExt_filt = np.copy(self.PressExt)
        self.PressExt_filt[np.abs(self.PressExt_filt)>self.PressExtMaxPa] = np.nan
        self.PressExt_filt[np.abs(self.PressExt_filt)==0] = np.nan
        self.PressExt_filt = Misc.interpolate_nans(self.PressExt_filt)
        print("")
        
        
    def update_masks(self,i):
        ''' Updates the masks for the coordinate grids.
            If calc is True, we calculate the mask based on the circular pupil. If not, we use the existing one and just update the grids. '''
        
        # We first create the grids for the pupils. Using x.data and y.data to not drag the previous mask.
        self.r = np.ma.MaskedArray(np.hypot((self.x.data-self.PupilXCenter[i]),(self.y.data-self.PupilYCenter[i])),mask=False)
        self.theta = np.ma.MaskedArray(np.arctan2((self.y.data-self.PupilYCenter[i]),(self.x.data-self.PupilXCenter[i])),mask=False)
        
        # Set/reset the masks - set True for any point which is either nan or outside of the unit disk
        # We MUST mask self.WF[i] based on self.WF[i].data, which is the "clean" version without the mask, 
        # (which may have been created in the past) otherwise there will be a cumulative masking operation.
        # This will cause the calculations to fail once we reset the pupil to pixels which were masked in the past.
        if self.use_pupil_for_mask[i]:
            self.PupilMask[i].fill(False)
            self.PupilMask[i][self.r>(self.PupilDiameter[i]/2)] = True
            self.PupilMask[i][np.isnan(self.WF[i].data)] = True
        
        # Remask the grids
        self.x.mask = self.PupilMask[i]
        self.y.mask = self.PupilMask[i]
        self.WF[i].mask = self.PupilMask[i]
        self.WFE[i].mask = self.PupilMask[i]
        
        self.r.mask = self.PupilMask[i]
        self.theta.mask = self.PupilMask[i]
        
        # Calculate the number of active pixels
        self.PupilActivePix[i] = np.size(self.WF[i].compressed())
        
        
    def process_frame_data(self,i,print_msgs=True):
        ''' Processes a single frame data within the pupil, by removing tilt and defocus (by fitting a plane and then a sphere). 
            It uses the result to calculate the RoC, f and d. It also decomposes the wavefront into Zernike polynomials.'''
        
        # We first start with updating the masks on the grid to match the current wavefront frame
        self.update_masks(i)

        # We only try to fit if there are enough active data pixels within our pupil or frame. Default is 9 (see init)
        if np.max((self.FrameActivePix[i],self.PupilActivePix[i])) >= self.ActivePix_min:
            exception_count = 0
            try:
                # No need to save the Zernike polynomials themselves, or the fit result, eats a lot of space
                Zp_i = model_Zernike(2*self.r/self.PupilDiameter[i],self.theta)
                fit_result = fit_Zernike_coefficients(Zernike_polynomials=Zp_i,Zdata=self.WF[i],MaxZerOrder=self.MaxZerOrder)
                self.Zc[i] = fit_result["Zc"]
                self.ZRsq[i] = fit_result["Rsq"]
                self.ZRsq_adj[i] = fit_result["Rsq_adj"]
            except:
                self.logprint("\nFrame "+str(i)+": Zernike decomposition failed, skipping the frame.",to_print=print_msgs)
                exception_count += 1
            try:
                WF_sphere_fit = fit_sphere(x = self.x*1e3,
                                                y = self.y*1e3,
                                                z = self.WF[i],
                                                z_unc = self.WF_unc)
                if WF_sphere_fit == {} or np.isnan(WF_sphere_fit["RoC"]) or np.isnan(WF_sphere_fit["RoC_unc_corr"]):
                    raise ValueError("");
                self.x0_WF[i] = WF_sphere_fit["x0"]/1e3
                self.y0_WF[i] = WF_sphere_fit["y0"]/1e3
                self.WFE[i] = WF_sphere_fit["z_diff"]
                self.RoC_WF[i] = WF_sphere_fit["RoC"]/1e3
                self.RoC_WF_unc[i] = WF_sphere_fit["RoC_unc_corr"]/1e3
            except:
                self.logprint("\nFrame "+str(i)+": Wavefront sphere fit failed, skipping the frame.",to_print=print_msgs)
                exception_count += 1
            try:
                WF_to_MUT_res = self.WF_to_MUT(RoC_WF = self.RoC_WF[i],d_SHWS = self.PupilDiameter[i])
                self.f_MUT[i] = WF_to_MUT_res["f_MUT"]
                if self.fmin != None and self.f_MUT[i] < self.fmin:
                    self.logprint("\nEstimated f_MUT of "+"{:.2f}".format(self.f_MUT[i])+" is smaller than the threshold, processing skipped.")
                    raise ValueError("")
                if self.fmax != None and self.f_MUT[i] > self.fmax:
                    self.logprint("\nEstimated f_MUT of "+"{:.2f}".format(self.f_MUT[i])+" is larger than the threshold, processing skipped.")
                    raise ValueError("")
                self.d_MUT[i] = WF_to_MUT_res["d_MUT"]
                self.dfMUT_dRoC_WF[i] = WF_to_MUT_res["dfMUT_dRoC_WF"]
                self.dd_MUT_dd_SHWS[i] = WF_to_MUT_res["dd_MUT_dd_SHWS"]
                self.f_MUT_unc[i] = self.RoC_WF_unc[i] * self.dfMUT_dRoC_WF[i]
                self.d_MUT_unc[i] = np.mean([self.x_scale,self.y_scale]) * self.dd_MUT_dd_SHWS[i]
                self.conj_mag[i] = self.d_MUT[i] / self.PupilDiameter[i]
                
            except:
                self.logprint("\nFrame "+str(i)+": WF to MUT translation failed, skipping the frame.",to_print=print_msgs)
                exception_count += 1
            self.processing_success[i] = (exception_count == 0)
        else:
            self.logprint("\nFrame "+str(i)+": too few data pixels, skipping the frame.",to_print=print_msgs)
            self.processing_success[i] = False
        

    def process_all(self,use_circ_mask=True):
        ''' Processes all frames '''
        for i in range(len(self.WF)):
            self.logprint("\rProcessing frame "+str(i)+"/"+str(self.frame_count-1)+" ("+"{:.0f}".format(100*i/(self.frame_count-1))+"%)",end="")
            sys.stdout.flush()
            self.process_frame_data(i,print_msgs=False)
        print("")
        
        
    def logprint(self,msg,to_print=True,end="\n"):
        ''' Logs and prints a message '''
        if type(msg) != str:
            raise TypeError("logprint: msg must be a string!")
        self.log+=(time.ctime()+" > "+msg)
        self.log+="\n"
        if to_print:
            print(msg,end=end)
            
            
    def time_to_frame(self,x):
        ''' Convert time to frame number '''
        return np.interp(x, self.time, np.arange(0,self.frame_count,1))
    def frame_to_time(self,x):
        ''' Convert frame number to time. Note that the conversion is not one-to-one since different frames might
            have the same time value if the timer has been reset or stopped. '''
        return np.interp(x, np.arange(0,self.frame_count,1), self.time)