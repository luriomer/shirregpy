# -*- coding: utf-8 -*-
"""
@ Omer Luria
Fluidic Technologies Laboratory
Technion - Israel Institute of Technology
luriomer@gmail.com , lomer@technion.ac.il

This code is used to analyze processed data (i.e., after it was pickled to a binary file by the RawDataProcessor.py)
using a dedicated GUI. It loads a processed data file, compares it to the correconding reconstruction and provides
further analysis tools.
"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button
import matplotlib.lines as mlines

import os.path
import sys
import ctypes
import plyer
import easygui
import time
from bisect import bisect_left
from DataHandler import DataHandler
from Reconstructor import Reconstructor
from WF_to_MUT import RoC_WF_to_f_d_MUT
from ModelFunctions import ZerNames
from PlotHandler import PlotHandler
import Misc
import pickle

plt.close("all")
plt.rcParams.update({"ytick.color" : "black",
                     "xtick.color" : "black",
                     "axes.labelcolor" : "black",
                     "axes.edgecolor" : "black",
                     "font.family" : "sans-serif",
                     "font.serif" : ["Calibri"],
                     "figure.figsize" : [16,9]})
plt.ion()

#%% Frame filter
def frame_is_valid(DataHandlerInstance,i):
    ''' Determines whether a certain frame is valid, basing the criteria on any available attribute.
        Returns True for valid frames, False for invalid frames.'''
    FrameActivePix_min = 200    # Minimal number of active pixels for the entire frame
    Ares_max = 10.1   # Max value for the resultant acceleration [g]
    DataHandlerInstance.valid_criteria = ""
    DataHandlerInstance.valid_criteria += "\n- At least "+str(FrameActivePix_min)+" active pixels"
    DataHandlerInstance.valid_criteria += "\n- Proper acceleration read below "+str(Ares_max)+"g in at least one of the IMUs"
    return ((DataHandlerInstance.FrameActivePix[i] >= FrameActivePix_min) and ((DataHandlerInstance.AccInt_res[i] <= Ares_max) or (DataHandlerInstance.AccExt_res_filt[i] <= Ares_max)))

#%% Initialize the data instance
load_filepath = plyer.filechooser.open_file(title="Choose a processed data file to load")
if load_filepath == []:
    print("No file was chosen, exiting.")
    sys.exit()
load_filepath = load_filepath[0]
print("\nLoading "+load_filepath+"...\n")
datafile = open(load_filepath,"rb")
ExpData = pickle.load(datafile)
print("Successfully loaded "+ExpData.name+".\nStarting GUI...\n")

ExpData.valid_frame_indices = []
ExpData.invalid_frame_indices = []

# Checking which frames are valid based on the frame filter
for i in range(ExpData.frame_count):
    if frame_is_valid(ExpData,i):
        ExpData.valid_frame_indices.append(i)
    else:
        ExpData.invalid_frame_indices.append(i)
        
ExpData.valid_frame_count = len(ExpData.valid_frame_indices)
ExpData.invalid_frame_count = len(ExpData.invalid_frame_indices)
if ExpData.valid_frame_count == 0:
    ctypes.windll.user32.MessageBoxW(0,"No valid frames to show, please review your data or the validity criteria.", "No valid frames to show", Misc.ICON_EXCLAIM)
    sys.exit("No valid frames to show, please review your data or the validity criteria.")

# We start with the first frame which is valid as well as successfully processed.
# This is a temporary patch until we can initiating the GUI with a failed reconstruction.
ExpData.BootFrame = ExpData.valid_frame_indices[0]
for i in ExpData.valid_frame_indices:
    if ExpData.processing_success[i]:
        ExpData.BootFrame=i
        break

if ExpData.BootFrame != ExpData.valid_frame_indices[0]:
    ctypes.windll.user32.MessageBoxW(0,"The first valid frame cannot be processed, skipping to the first processable one, consider going back if needed.", "Skipped first frame", Misc.ICON_INFO)

ExpData.update_masks(ExpData.BootFrame)
#%% Crete the Reconstruct instance and analyze the first valid frame
Recon = Reconstructor("Reconstructor",
                      x = (ExpData.x - ExpData.x0_WF[ExpData.BootFrame]) * ExpData.conj_mag[ExpData.BootFrame],
                      y = (ExpData.y - ExpData.y0_WF[ExpData.BootFrame]) * ExpData.conj_mag[ExpData.BootFrame],
                      x_mm = ExpData.x_mm * ExpData.conj_mag[ExpData.BootFrame],
                      y_mm = ExpData.y_mm * ExpData.conj_mag[ExpData.BootFrame],
                      WFE = ExpData.WFE[ExpData.BootFrame],
                      d = ExpData.d_MUT[ExpData.BootFrame],
                      f = ExpData.f_MUT[ExpData.BootFrame],
                      f_unc = ExpData.f_MUT_unc[ExpData.BootFrame],
                      f_eps = 1e-3,
                      delta_beta_eps = 1e-8,
                      lmd = ExpData.lmd,
                      WFE_unc = ExpData.WF_unc,
                      ImgPix = 128,
                      rigorous_response_analysis=False,
                      liquid_d0 = 50,
                      liquid_nu = 350,
                      liquid_rho = 970,
                      liquid_gamma = 20e-3,
                      liquid_h0 = 1.0,
                      liquid_Bo = 0,
                      EigenOrder_r = 10,
                      EigenOrder_theta = 10,
                      EigenVals_SearchRange = (1,1000,0.1))

Recon.calc_all(print_msgs=False)
#%% GUI functions
def Update(val):
    i = int(val)
    ExpData.update_masks(i)
    if ExpData.processing_success[i]:
        Recon.x = (ExpData.x - ExpData.x0_WF[i]) * ExpData.conj_mag[i]
        Recon.y = (ExpData.y - ExpData.y0_WF[i]) * ExpData.conj_mag[i]
        Recon.x_mm = ExpData.x_mm * ExpData.conj_mag[i]
        Recon.y_mm = ExpData.y_mm * ExpData.conj_mag[i]
        Recon.WFE = ExpData.WFE[i]
        Recon.d = ExpData.d_MUT[i]
        Recon.f = ExpData.f_MUT[i]
        Recon.f_unc = ExpData.f_MUT_unc[i]
        try:
            Recon.calc_all()
            ExpData.SurfRoC[i] = Recon.surf_sphere_fit_RoC
            ExpData.SurfErrRMS[i] = Recon.surf_sphere_fit_resid_rms
            ExpData.Surf_EigenFuncs_fit_AnBn[i] = Recon.surf_EigenFuncs_fit_AnBn
        except:
            Plotter.plot_invalid(i)
        else:
            Plotter.update_all_plots(i)
    else:
        Plotter.plot_invalid(i)
    
    if hasattr(Plotter,"EECirc"):
        Plotter.EECirc.remove()
        del(Plotter.EECirc)
        Plotter.PSFImg.axes.legend([Plotter.Red_line],["Airy disk"],framealpha=0.5)
        
    RefreshFigs()

def Back(event):
    if (FrameSlider.val>ExpData.valid_frame_indices[0]):
        if FrameSlider.val in ExpData.valid_frame_indices:
            FrameSlider.set_val(ExpData.valid_frame_indices[ExpData.valid_frame_indices.index(FrameSlider.val)-1])
        else:
            FrameSlider.set_val(ExpData.valid_frame_indices[bisect_left(ExpData.valid_frame_indices, FrameSlider.val)-1])
        
        ExpData.update_masks(FrameSlider.val)
        plt.show()
    
def Next(event):
    if (FrameSlider.val<ExpData.valid_frame_indices[-1]):
        if FrameSlider.val in ExpData.valid_frame_indices:
            FrameSlider.set_val(ExpData.valid_frame_indices[ExpData.valid_frame_indices.index(FrameSlider.val)+1])
        else:
            FrameSlider.set_val(ExpData.valid_frame_indices[bisect_left(ExpData.valid_frame_indices, FrameSlider.val)])
        ExpData.update_masks(FrameSlider.val)
        plt.show()
        
def GoTo(event):
    GoToFrame = easygui.enterbox("Insert the Frame you would like to go to (0-"+str(ExpData.frame_count-1)+")")
    if GoToFrame not in [None,""]:
        try:
            if GoToFrame.isdigit():
                number = int(float(GoToFrame))
                if (number>=0) and (number<=ExpData.frame_count-1):
                    FrameSlider.set_val(number)
                    if number not in ExpData.valid_frame_indices:
                        ctypes.windll.user32.MessageBoxW(0,"Selected frame "+GoToFrame+" was filtered as invalid.\nClick Next/Back to return to valid frames.", "Selected filtered frame", Misc.ICON_EXCLAIM)
                        next(event)
                    ExpData.update_masks(FrameSlider.val)
                    plt.show()
                else:
                    ctypes.windll.user32.MessageBoxW(0,"Frame "+GoToFrame+" is out of range.\nPlease type a number between 0 and "+str(ExpData.valid_frame_indices[-1]), "Invalid input", Misc.ICON_STOP)
            else:
                ctypes.windll.user32.MessageBoxW(0,"Frame must be an integer!", "Invalid input", Misc.ICON_EXCLAIM)
        except:
            pass
            #ctypes.windll.user32.MessageBoxW(0,"Frame must be an integer!", "Invalid input", Misc.ICON_EXCLAIM)
                    


def SetManPupil(event):
    pupil_input = easygui.multenterbox("Set pupil for reconstruction:","Pupil setting",["Pupil center X [mm] ("+"{:.2f}".format(ExpData.PupilXCenter[FrameSlider.val])+")",
                                                                                        "Pupil center Y [mm] ("+"{:.2f}".format(ExpData.PupilYCenter[FrameSlider.val])+")",
                                                                                        "Pupil diameter [mm] ("+"{:.2f}".format(ExpData.PupilDiameter[FrameSlider.val])+")",
                                                                                        "from frame ("+str(FrameSlider.val)+")",
                                                                                        "to frame ("+str(FrameSlider.val)+")"])
    SetManPupilXCenter = ExpData.PupilXCenter[FrameSlider.val]
    SetManPupilYCenter = ExpData.PupilYCenter[FrameSlider.val]
    SetManPupilDiameter = ExpData.PupilDiameter[FrameSlider.val]
    first_frame = FrameSlider.val
    last_frame = FrameSlider.val
    
    if pupil_input == None:
        return
    
    try:
        if pupil_input[0] not in [None,""]:
            SetManPupilXCenter = float(pupil_input[0])
    except:
        ctypes.windll.user32.MessageBoxW(0,"Pupil X center must be a real number!", "Invalid input", Misc.ICON_STOP)
    try:
        if pupil_input[1] not in [None,""]:
            SetManPupilYCenter = float(pupil_input[1]) 
    except:
        ctypes.windll.user32.MessageBoxW(0,"Pupil Y center must be a real number!", "Invalid input", Misc.ICON_STOP)
    try:
        if pupil_input[2] not in [None,""]:
            if float(pupil_input[2]) > 0:
                if float(pupil_input[2]) < 0.2:
                    ctypes.windll.user32.MessageBoxW(0,"Pupil diameter too small, set to min value of 0.2 mm at the origin", "Invalid pupil diameter", Misc.ICON_EXCLAIM)
                    SetManPupilDiameter = 0.2
                    SetManPupilXCenter = 0
                    SetManPupilYCenter = 0
                elif float(pupil_input[2]) > np.hypot(ExpData.x_mm,ExpData.y_mm):
                    ctypes.windll.user32.MessageBoxW(0,"Pupil diameter too large, set to the confining circle of "+"{:.1f}".format(np.hypot(ExpData.x_mm,ExpData.y_mm))+" mm at the origin.", "Invalid pupil diameter", Misc.ICON_EXCLAIM)
                    SetManPupilDiameter = np.hypot(ExpData.x_mm,ExpData.y_mm)
                    SetManPupilXCenter = 0
                    SetManPupilYCenter = 0
                else:
                    SetManPupilDiameter = float(pupil_input[2])
            else:
                ctypes.windll.user32.MessageBoxW(0,"Pupil radius must be positive!", "Invalid input", Misc.ICON_STOP)
    except:
        ctypes.windll.user32.MessageBoxW(0,"Pupil radius be a real number!", "Invalid input", Misc.ICON_STOP)
    # We only need to call the process_frame_data here, in the case where the pupil has changed. Otherwise nothing has changed.
    try:
        if pupil_input[3] not in [None,""]:
            first_frame = int(pupil_input[3])
            if first_frame < ExpData.valid_frame_indices[0]:
                ctypes.windll.user32.MessageBoxW(0,"The first frame is out of the valid frame range", "Invalid first frame", Misc.ICON_EXCLAIM)
        if pupil_input[4] not in [None,""]:
            last_frame = int(pupil_input[4])
            if last_frame > ExpData.valid_frame_indices[-1]:
                ctypes.windll.user32.MessageBoxW(0,"The last frame is out of the valid frame range", "Invalid last frame", Misc.ICON_EXCLAIM)
    except:
        ctypes.windll.user32.MessageBoxW(0,"The frame range must include only integers!", "Invalid input", Misc.ICON_STOP)
        return
    
    ExpData.logprint("Setting pupil of diameter "+str(round(SetManPupilDiameter,2))+" at ("+str(round(SetManPupilXCenter,2))+","+str(round(SetManPupilYCenter,2))+") between frames "+str(first_frame)+" - "+str(last_frame)+". Please wait...")
    for i in range(first_frame,last_frame+1):
        ExpData.use_pupil_for_mask[i] = True
        ExpData.PupilDiameter[i] = SetManPupilDiameter
        ExpData.PupilXCenter[i] = SetManPupilXCenter
        ExpData.PupilYCenter[i] = SetManPupilYCenter
        ExpData.process_frame_data(i)
    Update(FrameSlider.val)
    ExpData.logprint("Pupil setting operation completed successfully.")


def SetIntersectPupil(event):
    intersect_input = easygui.multenterbox("Enter parameters for intersecting pupil:","Frame range",["First frame ("+str(ExpData.valid_frame_indices[0])+")",
                                                                                                     "Last frame ("+str(ExpData.valid_frame_indices[-1])+")"])
    if intersect_input == None:
        return
    try:
        if intersect_input[0] == "":
            first_frame = ExpData.valid_frame_indices[0]
        else:
            first_frame = int(intersect_input[0])
            if first_frame < ExpData.valid_frame_indices[0]:
                ctypes.windll.user32.MessageBoxW(0,"The first frame is out of the valid frame range", "Invalid first frame", Misc.ICON_EXCLAIM)
        if intersect_input[1] == "":
            last_frame = ExpData.valid_frame_indices[-1]
        else:
            last_frame = int(intersect_input[1])
            if last_frame > ExpData.valid_frame_indices[-1]:
                ctypes.windll.user32.MessageBoxW(0,"The last frame is out of the valid frame range", "Invalid last frame", Misc.ICON_EXCLAIM)
    except:
        ctypes.windll.user32.MessageBoxW(0,"The frame range must include only integers!", "Invalid input", Misc.ICON_STOP)
        return
    ExpData.logprint("Setting intersection pupil between frames "+str(first_frame)+" - "+str(last_frame)+". Please wait...")
    intersect_mask = np.full_like(ExpData.WF[0].mask,False)
    for i in range(first_frame,last_frame+1):
        if i in ExpData.valid_frame_indices:
            intersect_mask = intersect_mask | np.isnan(ExpData.WF[i].data)
    (xcenter, ycenter), radius = Misc.find_smallest_circle(intersect_mask,ExpData.x.data,ExpData.y.data)
    for i in range(first_frame,last_frame+1):
        ExpData.use_pupil_for_mask[i] = False
        ExpData.PupilDiameter[i] = 2*radius
        ExpData.PupilXCenter[i] = xcenter
        ExpData.PupilYCenter[i] = ycenter
        ExpData.PupilMask[i] = intersect_mask
        ExpData.process_frame_data(i)
    Update(FrameSlider.val)
    ExpData.logprint("Pupil setting operation completed successfully.")
    
    
def BatchSurf(event):
    batch_input = easygui.multenterbox("Enter parameters for batch analysis:","Batch parameters",["First frame ("+str(ExpData.valid_frame_indices[0])+")",
                                                                              "Last frame ("+str(ExpData.valid_frame_indices[-1])+")"])
    if batch_input == None:
        return
    try:
        if batch_input[0] == "":
            first_frame = ExpData.valid_frame_indices[0]
        else:
            first_frame = int(batch_input[0])
            if first_frame < ExpData.valid_frame_indices[0]:
                ctypes.windll.user32.MessageBoxW(0,"The first frame is out of the valid frame range", "Invalid first frame", Misc.ICON_EXCLAIM)
        if batch_input[1] == "":
            last_frame = ExpData.valid_frame_indices[-1]
        else:
            last_frame = int(batch_input[1])
            if last_frame > ExpData.valid_frame_indices[-1]:
                ctypes.windll.user32.MessageBoxW(0,"The last frame is out of the valid frame range", "Invalid last frame", Misc.ICON_EXCLAIM)
    except:
        ctypes.windll.user32.MessageBoxW(0,"The frame range must include only integers!", "Invalid input", Misc.ICON_STOP)
        return
    
    # We build a subset of valid frames to analyze, just to provide correct progress indication
    first_valid_frame_ind = np.where(np.array(ExpData.valid_frame_indices)>=first_frame)[0][0]
    last_valid_frame_ind = np.where(np.array(ExpData.valid_frame_indices)<=last_frame)[0][-1]
    batch_frame_ind_arr = ExpData.valid_frame_indices[first_valid_frame_ind:last_valid_frame_ind+1]
    
    t1 = time.time()
    j = 0
    ExpData.logprint("\nRunning batch analysis between frames "+str(first_frame)+" - "+str(last_frame))
    for i in range(first_frame,last_frame+1):
        if i in ExpData.valid_frame_indices:
            ExpData.logprint("\nReconstructing surface frame "+str(i)+"/"+str(batch_frame_ind_arr[-1])+" ("+"{:.0f}".format(100*j/len(batch_frame_ind_arr))+"%)",end="")
            ExpData.update_masks(i)
            if ExpData.processing_success[i]:
                Recon.Zc = ExpData.Zc[i]
                Recon.d = ExpData.d_MUT[i]
                Recon.f = ExpData.f_MUT[i]
                Recon.x = (ExpData.x - ExpData.x0_WF[i]) * ExpData.conj_mag[i]
                Recon.y = (ExpData.y - ExpData.y0_WF[i]) * ExpData.conj_mag[i]
                Recon.x_mm = ExpData.x_mm * ExpData.conj_mag[i]
                Recon.y_mm = ExpData.y_mm * ExpData.conj_mag[i]
                Recon.WFE = ExpData.WFE[i]
                Recon.d = ExpData.d_MUT[i]
                Recon.f = ExpData.f_MUT[i]
                Recon.f_unc = ExpData.f_MUT_unc[i]
                try:
                    Recon.calc_all(print_msgs=False)
                    ExpData.SurfRoC[i] = Recon.surf_sphere_fit_RoC
                    ExpData.SurfErrRMS[i] = Recon.surf_sphere_fit_resid_rms
                    ExpData.Surf_EigenFuncs_fit_AnBn[i] = Recon.surf_EigenFuncs_fit_AnBn
                except:
                    ExpData.logprint("\nFrame "+str(i)+"/"+str(batch_frame_ind_arr[-1])+": reconstruction failed. Skipping.")
                j += 1
    t2 = time.time()
    ExpData.logprint("\n'"+ExpData.filename+"' surfaces reconstructed successfully in "+"{:.3f}".format(t2-t1)+" seconds.")
    ExpData.update_masks(FrameSlider.val)
    
    Plotter.update_all_plots(FrameSlider.val,autoscale=True)
    TimeEvolFig.canvas.draw()
    plt.show()
    
    
def EncircledEnergy(event):
    threshold = easygui.enterbox("Enter a threshold value for the normalized Encircled Energy\n(an apropriate circle will be indicated in the PSF):","Encircled Energy threshold input","0.838")
    if threshold not in [None,""]:
        try:
            threshold = float(threshold)
            if threshold < 0 or threshold > 1:
                ctypes.windll.user32.MessageBoxW(0,"Threshold value must be between 0 - 1. The default value of 0.838 will be used.", "Invalid input", Misc.ICON_STOP)
                threshold = 0.838
        except:
            ctypes.windll.user32.MessageBoxW(0,"Threshold value must be a positive real number! The default value of 0.838 will be used.", "Invalid input", Misc.ICON_STOP)
            threshold = 0.838
    
        Recon.calc_EE()
        EEfig = plt.figure()
        Plotter.plot_EE(EEfig,111,threshold=threshold)
        # We plot the circle at the denormalized centroid of the PSF. The minus signs are used since
        # since imshow flips the orientation.
        Plotter.EECirc = plt.Circle((Recon.x0_psf,Recon.y0_psf),Recon.rEE_interp(threshold),fill=False,edgecolor="blue",linestyle='--',linewidth=1.5)
        Plotter.PSFImg.axes.add_artist(Plotter.EECirc)
        Plotter.Blue_line = mlines.Line2D([], [], color='blue', linestyle='--')
        Plotter.PSFImg.axes.legend([Plotter.Red_line,Plotter.Blue_line],["Airy disk",str(threshold*100)+"% Encircled Energy"],framealpha=0.5)
        WFFig.canvas.draw()


def PickleData(event):
    pending_savefile_selection = True
    while pending_savefile_selection:
        save_filepath = plyer.filechooser.save_file(title="\nChoose a filename to pickle the processed data",filters=["*pkl"])[0]
        if save_filepath == []:
            print("No file was chosen, aboring pickle.")
            return()
        if save_filepath[-4:] not in [".pkl",".PKL"]:
            save_filepath+=".pkl"
            
        if os.path.exists(save_filepath):
            overwrite_question = ctypes.windll.user32.MessageBoxW(0, save_filepath.split('\\')[-1]+" exists.\nWould you like to overwrite it?", "Confirm overwrite", Misc.MB_YESNO | Misc.ICON_QUESTION)
            if overwrite_question == 6:
                pending_savefile_selection = False
        else:
            pending_savefile_selection = False
    ExpData.processed_filepath = save_filepath
    ExpData.logprint("\nPickeling to '"+ExpData.processed_filepath+"'. Please wait...")
    processed_file = open(ExpData.processed_filepath,"wb")
    pickle.dump(ExpData,processed_file)
    processed_file.close()
    print("Pickle operation completed successfully.")


def RefreshFigs():
    if 'EnvFig' in globals():
        EnvFig.canvas.draw()
        EnvFig.canvas.flush_events()
    if 'WFFig' in globals():
        WFFig.canvas.draw()
        WFFig.canvas.flush_events()
    if 'SurfFig' in globals():
        SurfFig.canvas.draw()
        SurfFig.canvas.flush_events()
    if 'TimeEvolFig' in globals():
        TimeEvolFig.canvas.draw()
        TimeEvolFig.canvas.flush_events()
    plt.show()


plt.close("all")
summary_str = "Analyzing "+str(ExpData.valid_frame_count)+" / "+str(ExpData.frame_count)+" frames.\n"+str(ExpData.invalid_frame_count)+" frames where filtered based on the following criteria:\n"+ExpData.valid_criteria
ctypes.windll.user32.MessageBoxW(0,summary_str, "Processing summary", Misc.ICON_INFO)

Plotter = PlotHandler(ExpData,Recon,fontsize=12)

WFFig = plt.figure("Main Panel")
WFspec = WFFig.add_gridspec(nrows=2,ncols=3)

Plotter.plot_WF(WFFig, WFspec[0,0])
Plotter.plot_Zernikes(WFFig, WFspec[0,1:])
Plotter.plot_WFE(WFFig, WFspec[1,0],interp_method="none")
Plotter.plot_PSF(WFFig, WFspec[1,1],interp_method="bilinear")
Plotter.plot_MTF(WFFig, WFspec[1,2],interp_method="bilinear")

PickleBox = plt.axes([0.02, 0.02, 0.05, 0.025])
PickleButton = Button(PickleBox, 'Pickle', hovercolor='0.975')
PickleButton.on_clicked(PickleData)

ManPupilBox = plt.axes([0.08, 0.02, 0.06, 0.025])
ManPupilButton = Button(ManPupilBox, 'Manual Pupil', hovercolor='0.975')
ManPupilButton.on_clicked(SetManPupil)

IntersectPupilBox = plt.axes([0.15, 0.02, 0.07, 0.025])
IntersectPupilButton = Button(IntersectPupilBox, 'Interesct Pupil', hovercolor='0.975')
IntersectPupilButton.on_clicked(SetIntersectPupil)

BatchSurfBox = plt.axes([0.23, 0.02, 0.07, 0.025])
BatchSurfButton = Button(BatchSurfBox, 'Batch Surface', hovercolor='0.975')
BatchSurfButton.on_clicked(BatchSurf)

EEBox = plt.axes([0.31, 0.02, 0.08, 0.025])
EEButton = Button(EEBox, 'Encircled Energy', hovercolor='0.975')
EEButton.on_clicked(EncircledEnergy)

Backframe = plt.axes([0.43, 0.02, 0.05, 0.025])
BackButton = Button(Backframe, '< Back', hovercolor='0.975')
BackButton.on_clicked(Back)

FrameSliderBox = plt.axes([0.535, 0.005, 0.3, 0.05])
FrameSlider = Slider(ax=FrameSliderBox, label='Frame', valmin=0, valmax=ExpData.frame_count-1, valinit=ExpData.BootFrame,valstep=1,valfmt="%1.0f")
FrameSlider.on_changed(Update)

NextFrame = plt.axes([0.875, 0.02, 0.05, 0.025])
NextButton = Button(NextFrame, 'Next >', hovercolor='0.975')
NextButton.on_clicked(Next)

GoToBox = plt.axes([0.942, 0.02, 0.05, 0.025])
GoToButton = Button(GoToBox, 'Go To', hovercolor='0.975')
GoToButton.on_clicked(GoTo)

WFFig.subplots_adjust(top=0.92,bottom=0.11,left=0.0,right=0.992,hspace=0.37,wspace=0.2)

SurfFig = plt.figure("Surface Reconstruction",layout="constrained")
Plotter.plot_recon_surface(SurfFig)

#EnvFig = plt.figure("Environmental data",layout="constrained")
#Plotter.plot_Env_data(EnvFig,ExpData.BootFrame)

TimeEvolFig = plt.figure("Time Evolution",layout="constrained")
Plotter.plot_time_evolution(TimeEvolFig,ExpData.BootFrame)

Update(FrameSlider.val)
plt.show()