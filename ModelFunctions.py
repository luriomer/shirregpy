# -*- coding: utf-8 -*-
"""
@ Omer Luria
Fluidic Technologies Laboratory
Technion - Israel Institute of Technology
luriomer@gmail.com , lomer@technion.ac.il

This code holds all the tools related to model functions and curve fitting: Zernike polynomials, spheres, planes etc.
"""
import numpy as np
import scipy as sp
import numdifftools as nd
import matplotlib.pyplot as plt
import Misc

#%% Models
def model_plane(params,args):
    ''' Returns a tilted and biased plane.
        params must be a 1D array in the form (a,b,z0)
        model_args is a dictionary of {"x":x, "y":y} '''
    a = params[0]
    b = params[1]
    z0 = params[2]
    
    x = args["x"]
    y = args["y"]
    
    return a*x + b*y + z0


def model_sphere(params,args):
    ''' Returns a spherical cap surface. Because a sphere is not a function, we separate it into either a concave or convex surface.
        We do that by replacing those points with nans and masking.
        params must be a 1D array in the form (RoC,x0,y0,z0)
        model_args is a dictionary of {"x":x, "y":y, "shape":"concave" or "convex"} '''
    RoC = params[0]
    x0 = params[1]
    y0 = params[2]
    z0 = params[3]
    
    x = args["x"]
    y = args["y"]
    shape = args["shape"]
    

    l = RoC**2 - (x-x0)**2 - (y-y0)**2
    
    '''
    if hasattr(l,"mask"):
        orig_masked = np.sum(l.mask)
        l.mask[l<0]=True
        l = l.filled(np.nan)
        l = np.ma.masked_invalid(l)
        if np.all(l.mask):
            return np.nan
        if np.sum(l.mask) > orig_masked:
            print("model_sphere: masking points outside of the domain.")
    else:
        l[l<0]=np.nan
        l = np.ma.masked_invalid(l)
        if np.all(l.mask):
            return np.nan
        if np.any(l.mask):
            print("model_sphere: masking points outside of the domain.")
    '''
    
    # No need to replace negative values of l to nans, np.sqrt does that automatically, but it warns.
    # To not see that warning, we simply disable that specific warning for those lines.
    if shape == "convex":
        with np.errstate(invalid="ignore"):
            res =  z0 + np.sqrt(l)
    elif shape == "concave":
        with np.errstate(invalid="ignore"):
            res = z0 - np.sqrt(l)
    else:
        raise ValueError("shape invalid; must be either concave or convex!")
    
    if hasattr(res,"mask"):
        if np.all(res.mask) or np.all(np.isnan(res)):
            raise ValueError("All values are nan, the sphere is ouside of the domain.")
    return res


def model_paraboloid(params,args):
    ''' Return a circular paraboloid surface.
    params is simply [f]
    args is {"r":r} '''
    
    f = params[0]
    r = args["r"]
    
    # We saturate f at extremely small values to prevent division by zero
    if np.abs(f) < 1e-10:
        f = np.sign(f) * 1e-10
    
    return r**2 /4 /f


def model_Zernike(r,theta):
    ''' Calculate the first 36 Zernikes polynomials on an (r,theta) meshgrid.
        Zernike nomenclature according to OSA/ANSI Z80.28-2017 
        (see preliminary table at https://en.wikipedia.org/wiki/Zernike_polynomials#Zernike_polynomials).
        The function returns every polynomial separately, so the returned shape is (Npoly,Nr,Ntheta).
        We also make sure everything outside the unit radius is zero.'''
    Zps = [None]*36#np.zeros([36]+list(np.shape(r)))
    Zps[0] = (np.sqrt(1)   *(np.cos(theta)**2+np.sin(theta)**2))                 #Piston
    Zps[1] = (np.sqrt(4)   * r * np.sin(theta))                                  #Tip Y
    Zps[2] = (np.sqrt(4)   * r * np.cos(theta))                                  #Tilt X
    Zps[3] = (np.sqrt(6)   * r**2 * np.sin(2*theta))                             #Astig. Y
    Zps[4] = (np.sqrt(3)   * (2*r**2 - 1))                                       #Defocus
    Zps[5] = (np.sqrt(6)   * r**2 * np.cos(2*theta))                             #Astig. X
    Zps[6] = (np.sqrt(8)   * r**3 * np.sin(3*theta))                             #Trefoil Y
    Zps[7] = (np.sqrt(8)   * (3*r**2 - 2) * r * np.sin(theta))                   #Coma X
    Zps[8] = (np.sqrt(8)   * (3*r**2 - 2) * r * np.cos(theta))                   #Coma Y
    Zps[9] = (np.sqrt(8)   * r**3 * np.cos(3*theta))                             #Trefoil X
    Zps[10] = (np.sqrt(10) * r**4 * np.sin(4*theta))                             #Tetrafoil Y
    Zps[11] = (np.sqrt(10) * (4*r**2 - 3) * r**2 * np.sin(2*theta))              #2nd Astig. Y
    Zps[12] = (np.sqrt(5)  * (6*r**4 - 6 * r**2 + 1))                            #Primary Sph
    Zps[13] = (np.sqrt(10) * (4*r**2 - 3) * r**2 * np.cos(2*theta))              #2nd Astig. X
    Zps[14] = (np.sqrt(10) * r**4 * np.cos(4*theta))                             #Tetrafoil X
    Zps[15] = (np.sqrt(12) * r**5 * np.sin(5*theta))                             #Pentafoil Y
    Zps[16] = (np.sqrt(12) * (5*r**5 - 4*r**3) * np.sin(3*theta))                #2nd Trefoil Y
    Zps[17] = (np.sqrt(12) * (10*r**5 - 12*r**3 + 3*r) * np.sin(theta))          #2nd Coma Y
    Zps[18] = (np.sqrt(12) * (10*r**5 - 12*r**3 + 3*r) * np.cos(theta))          #2nd Coma X
    Zps[19] = (np.sqrt(12) * (5*r**5 - 4*r**3) * np.cos(3*theta))                #2nd Trefoil X
    Zps[20] = (np.sqrt(12) * r**5 * np.cos(5*theta))                             #Pentafoil X
    Zps[21] = (np.sqrt(14) * r**6 * np.sin(6*theta))                             #Hexafoil Y
    Zps[22] = (np.sqrt(14) * (6*r**6 - 5*r**4) * np.sin(4*theta))                #2nd Tetrafoil Y
    Zps[23] = (np.sqrt(14) * (15*r**6 - 20*r**4 + 6*r**2) * np.sin(2*theta))     #3rd Astig. Y
    Zps[24] = (np.sqrt(7)  * (20*r**6 - 30*r**4 + 12*r**2 - 1))                  #2nd Sph
    Zps[25] = (np.sqrt(14) * (15*r**6 - 20*r**4 + 6*r**2) * np.cos(2*theta))     #3rd Astig. X
    Zps[26] = (np.sqrt(14) * (6*r**6 - 5*r**4) * np.cos(4*theta))                #2nd Tetrafoil X
    Zps[27] = (np.sqrt(14) * r**6 * np.cos(6*theta))                             #Hexafoil X
    Zps[28] = (np.sqrt(16) * r**7 * np.sin(7*theta))                             #Heptafoil Y
    Zps[29] = (np.sqrt(16) * (7*r**7 - 6*r**5) * np.sin(5*theta))                #2nd Pentafoil Y
    Zps[30] = (np.sqrt(16) * (21*r**7 - 30*r**5 + 10*r**3) * np.sin(3*theta))    #3rd Trefoil Y
    Zps[31] = (np.sqrt(16) * (35*r**7 - 60*r**5 + 30*r**3 - 4*r) *np.sin(theta)) #3rd Coma Y
    Zps[32] = (np.sqrt(16) * (35*r**7 - 60*r**5 + 30*r**3 - 4*r) *np.cos(theta)) #3rd Coma X
    Zps[33] = (np.sqrt(16) * (21*r**7 - 30*r**5 + 10*r**3) * np.cos(3*theta))    #3rd Trefoil X
    Zps[34] = (np.sqrt(16) * (7*r**7 - 6*r**5) * np.cos(5*theta))                #2nd Pentafoil X
    Zps[35] = (np.sqrt(16) * r**7 * np.cos(7*theta))                             #Heptafoil X
    
    # Nullify everything outside the unit disk
    mask = np.full_like(r,False)
    mask[r>1] = True
    for i in range(len(Zps)):
        Zps[i][r>1] = 0
        Zps[i] = np.ma.MaskedArray(Zps[i],mask=mask)
    return Zps

ZerNames = ["Piston","Tip Y","Tilt X",
            "Astig. Y","Defocus","Astig. X",
            "Trefoil Y","Coma X","Coma Y","Trefoil X","Tetrafoil Y",
            "2nd Astig. Y","Primary Sph","2nd Astig. X","Tetrafoil X",
            "Pentafoil Y","2nd Trefoil Y","2nd Coma Y","2nd Coma X","2nd Trefoil X",
            "Pentafoil X","Hexafoil Y","2nd Tetrafoil Y","3rd Astig. Y","2nd Sph",
            "3rd Astig. X","2nd Tetrafoil X","Hexafoil X","Heptafoil Y","2nd Pentafoil Y",
            "3rd Trefoil Y","3rd Coma Y","3rd Coma X","3rd Trefoil X","2nd Pentafoil X","Heptafoil X"]


def calc_LiquidDynamics_SpatialEigenVals(x,Bo):
    ''' Calculates the spatial eigenvalues, marked as alpha_nm and beta_nm in the paper.
        See Gabay et al, XXX
        Parameters:
            x: the eigenvalues input array
            Bo: Bond number 
        Returns:
            A tuple of (alpha_nm,beta_nm) '''
    alpha_nm = np.sqrt((np.sqrt(Bo**2 + 4*x**4) - Bo) /2)
    beta_nm = np.sqrt((np.sqrt(Bo**2 + 4*x**4) + Bo) /2)
    
    return (alpha_nm,beta_nm)


def calc_LiquidDynamics_EigenVals(EigenOrder_r,EigenOrder_theta,Bo,search_range):
    ''' Calculate the eigen values for the 2D fixed-volume pinned liquid film dynamic problem in cylindrical coordinates.
        For more info see Gabay et al XXX (TBD) 
        Input:
            EigenOrder_r - number of eigenvalues in the radial direction
            EigenOrder_theta - number of eigenvalues in the asimuthal direction
            Bo - the Bond number
            search_range - range for initial guess to search for eigenvalues by zero crossing identification. Format of (start,stop,step) to pass to np.arange.
        Output: eigenvalues - float array of size (EigenOrder_theta,EigenOrder_r) '''

    EigenVals = np.zeros([EigenOrder_theta, EigenOrder_r])

    # Define the range where we look for eigenvalues
    search_range = np.arange(search_range[0], search_range[1]+search_range[2], search_range[2])
    
    Gamma_I , Gamma_J = calc_LiquidDynamics_SpatialEigenVals(x=search_range,Bo=Bo)
    
    for i in range(EigenOrder_theta):
        EigenValsign = np.sign(Gamma_I * sp.special.jve(i-1, Gamma_J) + 
            (Gamma_J * sp.special.ive(i-1, Gamma_I) / sp.special.ive(i, Gamma_I) - i * (Gamma_I**2 + Gamma_J**2) / (Gamma_I * Gamma_J)) * sp.special.jve(i, Gamma_J))

        # Remove NaNs and find zero crossings in EigenValsign
        EigenValsign = EigenValsign[~np.isnan(EigenValsign)]
        zero_cross_ind = np.where(np.diff(EigenValsign))[0]
        
        for j in range(EigenOrder_r):
            def characteristic_equation(x, args=Bo):
                #bi_val = np.sqrt((np.sqrt(Bo**2 + 4*y**4) + Bo) / 2)
                #bj_val = np.sqrt((np.sqrt(Bo**2 + 4*y**4) - Bo) / 2)
                bj_val, bi_val = calc_LiquidDynamics_SpatialEigenVals(x=x,Bo=Bo)
                return (bi_val * sp.special.jve(i-1, bj_val) +
                        sp.special.jve(i, bj_val) * (bj_val * (sp.special.ive(i-1, bi_val) / sp.special.ive(i, bi_val)) - np.sqrt(i**2 * (Bo**2 + 4*x**4) / x**4)))
            
            if zero_cross_ind[j] == 0:
                xi = [search_range[zero_cross_ind[j+1]-5], search_range[zero_cross_ind[j+1]+5]]
            else:
                xi = [search_range[zero_cross_ind[j]-5], search_range[zero_cross_ind[j]+5]]
            
            EigenVals[i, j] = sp.optimize.fsolve(characteristic_equation, xi)[0]
    
    return EigenVals


def calc_LiquidDynamics_EigenFuncs(r,theta,EigenVals,Bo):
    ''' Calculates the eigen functions on a specified r,theta grid.
        Parameters:
            r - 2D meshgrid of the normalized radial coordinate
            theta - 2D meshgrid of the azimuthal coordinate
            EigenVals - 2D array of the eigen values
            Bo - Bond number
        
        Returns:
            EigenFuncs - array of arrays for the all the eigenfunctions, evaluated on the (r,theta) meshgrid.'''

    if np.shape(r) != np.shape(theta):
        raise ValueError("r and theta must be of the same shape!")
    grid_height,grid_width = np.shape(r)
    EigenOrder_theta , EigenOrder_r = np.shape(EigenVals)
    
    # Prepare the eigen functions data structure - a nested list of arrays
    # this is the correct way and NOT a multidimensional array since we need 2d array of 2d MASKED arrays.
    # (if we try to put masked arrays into regular arrays, the mask is omitted).
    EigenFuncs = []
    for i in range(2*EigenOrder_theta):
        EigenFuncs.append([])
        for j in range(EigenOrder_r+1):
            EigenFuncs[i].append(np.zeros_like(r))
    
    alpha_nm,beta_nm = calc_LiquidDynamics_SpatialEigenVals(x=EigenVals,Bo=Bo)
    
    if Bo == 0:
        R00 = 1-r**2
    else:
        R00 = 2*(sp.special.iv(0,np.sqrt(Bo))/sp.special.iv(2,np.sqrt(Bo)))*(1 - (1/sp.special.iv(0,np.sqrt(Bo)))*sp.special.iv(0,np.sqrt(Bo)*r))
    
    EigenFuncs[0][0] = R00
    for i in range(EigenOrder_theta):
        ThC = np.cos(i*theta)
        ThS = np.sin(i*theta)
        for j in range(EigenOrder_r):
            Rnm = sp.special.jv(i,alpha_nm[i,j]*r)-(sp.special.jv(i,alpha_nm[i,j])/sp.special.ive(i,beta_nm[i,j]))*sp.special.ive(i,beta_nm[i,j]*r)*np.exp(beta_nm[i,j]*(r-1))
            EigenFuncs[2*i][j+1] = Rnm * ThC
            EigenFuncs[2*i+1][j+1] = Rnm * ThS
    
    return EigenFuncs


#%% Chi squared tools for uncertainty analysis
def chi_squared(model_params,model_func,model_args,data,data_unc):
    ''' Returns the chi squared estimator for a data set and apropriate model.
        model_params and model_args match what model_func expects to receive.
        data_unc must be either a scalar or an array with the same shape as data. '''
    DoF = np.size(data) - np.size(model_params) - 1
    return 1/DoF * np.sum(((model_func(model_params,model_args)-data)/(data_unc))**2)


def chi_squared_Hessian(model_params,model_func,model_args,data,data_unc):
    ''' Returns the Hessian for a data set and apropriate model.
        model_params and model_args match what model_func expects to receive.
        data_unc must be either a scalar or an array with the same shape as data. '''
    H = nd.Hessian(chi_squared)
    return H(model_params,model_func,model_args,data,data_unc)


#%% Fitting operations
def fit_Zernike_coefficients(Zernike_polynomials, Zdata, MaxZerOrder=36):
    ''' Fits data array to Zernike polynomials by linear least squares, resulting in the Zernike Coefficients.
        Zernike_Polynomials should be a list of Npoly number of arrays, each in the shape (Nr,Ntheta)
        Zdata should have the shape (Nr,Ntheta) '''
    Zdata = Zdata.compressed()
    A = np.zeros((len(Zdata),MaxZerOrder))
    for i in range(MaxZerOrder):
        A[:,i] = Zernike_polynomials[i].compressed()
    res_coeffs, residuals, rank, singulars = sp.linalg.lstsq(A,Zdata)
    
    Rsq = 1 - residuals/np.sum((Zdata-np.mean(Zdata))**2)
    Rsq_adj = 1-(1-Rsq)*((len(Zdata)-1)/(len(Zdata)-MaxZerOrder-1))
    
    return {"Zc" : res_coeffs,
            "Rsq" : Rsq,
            "Rsq_adj" : Rsq_adj}


def fit_LiquidDynamics_EigenFuncs(z,EigenFuncs):
    ''' Fits data array to the eigenfunctions of the 2D liquid dynamics problem in a circular domain.
        Parameters:
            r: normalized radial coordinate meshgrid 2d masked array. r must be 0<=r<=1 where r=1 corresponds to the liquid pinning dish.
            theta: azimuthal coordinate meshgrid 2d masked array.
            z: surface topography at (r,theta), 2d masked array.
            EigenFuncs: the eigenfunctions to which we seek to decompose z.
        Returns:
            A dictionary containing the linear least squares fit decomposition. '''
    
    # Preparing the coefficient matrix we eventually return
    AnBn = np.zeros((len(EigenFuncs),len(EigenFuncs[0])))
    
    # Flattering the data and assigning a data matrix
    if hasattr(z,"mask"):
        z_flat = z.compressed()
    else:
        z_flat = z.flatten()
    
    B = np.zeros((len(z_flat),1))
    B[:,0] = z_flat
    
    # First count how many nonzero eigenfunctions we have
    EigenFuncs_num = []
    for j in range(len(EigenFuncs[0])):
        k = 0
        for i in range(len(EigenFuncs)):
            if np.any(EigenFuncs[i][j]):
                k += 1
        EigenFuncs_num.append(k)
    EigenFuncs_num_total = np.sum(EigenFuncs_num)
    # We build the A matrix by unpacking the eigenfunctions column after column, each time fitting for all the columns.
    # We eventually pack and return it shaped like EigenFuncs.
    # We only take the functions which are nonzero - otherwise we'll have linearly dependent columns, which might mess up the fit.
    # So we record which eigenfucntions where skipped, and we account for that later on.
    
    # The fit itself is done sequentially (sequantil least squares), each time fitting a single radial order (with all azimuthal orders), and fitting next order with the residual.
    for j in range(len(EigenFuncs[0])):
        A = np.zeros((len(z_flat),EigenFuncs_num[j]))
        zero_eigenfuncs_skipped_ind = []
        k = 0
        for i in range(len(EigenFuncs)):
            if np.any(EigenFuncs[i][j]):
                if hasattr(EigenFuncs[i][j],"mask"):
                    A[:,k] = EigenFuncs[i][j].compressed()
                else:
                    A[:,k] = EigenFuncs[i][j].flatten()
                k += 1
            else:
                zero_eigenfuncs_skipped_ind.append((i,j))
        coeffs, residuals, rank, singulars = sp.linalg.lstsq(A,B)
        # After running the fit, we now populate the coefficients matrix which will correspond to the same outer shape as EigenFuncs.
        # Since the fit is sequential this operation is also done column-by-column.
        k = 0
        comulative_values = np.zeros_like(z_flat)
        for i in range(len(EigenFuncs)):
            if (i,j) in zero_eigenfuncs_skipped_ind:
                AnBn[i,j] = np.nan
            else:
                AnBn[i,j] = coeffs[k][0]
                comulative_values += AnBn[i,j] * A[:,k]
                k += 1
        B[:,0] -= comulative_values
    
    # We eventually mask all the invalid coefficients and return the last residual results.
    AnBn = np.ma.masked_invalid(AnBn)
    if np.size(residuals) > 0:
        Rsq = 1 - residuals[0]/np.sum((z_flat-np.mean(z_flat))**2)
        Rsq_adj = 1-(1-Rsq)*((len(z_flat)-1)/(len(z_flat)-EigenFuncs_num_total-1))
    else:
        Rsq = None
        Rsq_adj = None
    
    return {"AnBn" : AnBn,
            "residuals":residuals,
            "rank":rank,
            "singulars":singulars,
            "Rsq" : Rsq,
            "Rsq_adj" : Rsq_adj}

def fit_plane(x,y,z,z_unc):
    ''' Fit a plane to data using linear least squares.
        x and y must be 2D meshgrids.
        x,y,z dimensions must be the same! Output dimensions are unchanged.
        z_unc is the typical uncertainty in z, for uncertaintly analysis. '''
    if hasattr(x,"mask"):
        if hasattr(y,"mask") and hasattr(z,"mask"):
            x_flat = x.compressed()
            y_flat = y.compressed()
            z_flat = z.compressed()
        else:
            raise ValueError("x,y,z must be consistently masked! Check your data.")
    else:
        if hasattr(y,"mask") or hasattr(z,"mask"):
            raise ValueError("x,y,z must be consistently masked! Check your data.")
        else:
            x_flat = x.flatten()
            y_flat = y.flatten()
            z_flat = z.flatten()

    if type(z_unc) == np.ma.MaskedArray:
        z_unc_flat = z_unc.compressed()
    elif type(z_unc) == np.array:
        z_unc_flat = z_unc.flatten()
    else:
        z_unc_flat = z_unc

    # Assemble the A matrix
    A = np.zeros((len(x_flat),3))
    A[:,0] = x_flat
    A[:,1] = y_flat
    A[:,2] = 1
    
    # Assemble the B matrix
    B = np.zeros((len(x_flat),1))
    B[:,0] = z_flat
    
    # Call linear least squares
    C, residuals, rank, singval = np.linalg.lstsq(A,B,rcond=None)
    # Calculating resulting parameters
    a = C[0][0]
    b = C[1][0]
    z0 = C[2][0]
    
    DoF = len(x_flat)-3-1
    R_sq = 1 - residuals[0]/np.sum((B-np.mean(B))**2)
    R_sq_adj = 1-(1-R_sq)*((len(x_flat)-1)/DoF)
    
    z_plane = model_plane(params=[a,b,z0], args={"x":x,"y":y})
    z_diff = z - z_plane
    z_diff_rms = np.std(z_diff)
    '''
    H = chi_squared_Hessian(model_params=[a,b,z0],
                            model_func=model_plane,
                            model_args={"x":x_flat,"y":y_flat},
                            data=z_flat,
                            data_unc = z_unc_flat)
    E = np.linalg.inv(H)
    
    # Taken from the chi squared statistic for p-value of 95% confidence (2sigma) with v=3 fitted parameters.
    # For further details see Numerical Recipes p.815.
    delta_chi_sq_nu = 8.02
    # Fully correlated errors, assuming normal distribution
    a_unc_corr = np.sqrt(np.abs(E[0,0])) * np.sqrt(delta_chi_sq_nu)
    b_unc_corr = np.sqrt(np.abs(E[1,1])) * np.sqrt(delta_chi_sq_nu)
    z0_unc_corr = np.sqrt(np.abs(E[2,2])) * np.sqrt(delta_chi_sq_nu)
    '''
    return {"a" : a,
            #"a_unc" : a_unc_corr,
            "b" : b,
            #"b_unc" : b_unc_corr,
            "z0" : z0, 
            #"z0_unc" : z0_unc_corr,
            "R_sq" : R_sq,
            "R_sq_adj" : R_sq_adj,
            "z_plane" : z_plane,
            "z_diff" : z_diff,
            "z_diff_rms" : z_diff_rms}


def fit_sphere(x,y,z,z_unc):
    ''' Fit an exact sphere to data using linear least squares in the form of Af=c
        Based on Charles F. Jekel's code (https://jekel.me/2015/Least-Squares-Sphere-Fit/),
        input x and y must be 2D meshgrids, z must be 2D as well
        x,y,z dimensions must be the same! Output dimensions are unchanged
        z_unc is the typical uncertainty in z, for uncertaintly analysis. '''
    
    if hasattr(x,"mask"):
        if hasattr(y,"mask") and hasattr(z,"mask"):
            x_flat = x.compressed()
            y_flat = y.compressed()
            z_flat = z.compressed()
        else:
            raise TypeError("x,y,z must be consistently masked! Check your data.")
    else:
        if hasattr(y,"mask") or hasattr(z,"mask"):
            raise TypeError("x,y,z must be consistently masked! Check your data.")
        else:
            x_flat = x.flatten()
            y_flat = y.flatten()
            z_flat = z.flatten()

    if type(z_unc) == np.ma.MaskedArray:
        z_unc_flat = z_unc.compressed()
    elif type(z_unc) == np.array:
        z_unc_flat = z_unc.flatten()
    else:
        z_unc_flat = z_unc
    # Assemble the A matrix
    A = np.zeros((len(x_flat),4))
    A[:,0] = 2*x_flat
    A[:,1] = 2*y_flat
    A[:,2] = 2*z_flat
    A[:,3] = 1

    # Assemble the B matrix
    B = np.zeros((len(x_flat),1))
    B[:,0] = x_flat**2 + y_flat**2 + z_flat**2
    
    # Call linear least squares
    C, residuals, rank, singval = np.linalg.lstsq(A,B,rcond=None)
    
    # Calculating resulting parameters
    x0 = C[0][0]
    y0 = C[1][0]
    z0 = C[2][0]
    RoC = np.sqrt(x0**2 + y0**2 + z0**2 + C[3][0])
    
    DoF = len(x_flat)-4-1
    R_sq = 1 - residuals[0]/np.sum((B-np.mean(B))**2)
    R_sq_adj = 1-(1-R_sq)*(len(x_flat)-1)/DoF
    
    # Decide which portion to take - caonvex or cancave - based on the RMS
    # This is becuase we use linear least squares, both options satisfy the same fit but do not result in the same error.
    z_sph_concave = model_sphere(params = [RoC,x0,y0,z0],
                           args = {"x":x,"y":y,"shape":"concave"})
        
    z_diff_concave = z - z_sph_concave
    z_diff_rms_concave = np.std(z_diff_concave)
    
    z_sph_convex = model_sphere(params = [RoC,x0,y0,z0],
                           args = {"x":x,"y":y,"shape":"convex"})
    
    # If model_sphere returns nan, it means the sphere is outside of the grid, return an empty dictionary
    if np.all(np.isnan(z_sph_concave)) or np.all(np.isnan(z_sph_convex)):
        return {}
    
    z_diff_convex = z - z_sph_convex
    z_diff_rms_convex = np.std(z_diff_convex)

    if z_diff_rms_concave < z_diff_rms_convex:
        z_sph = z_sph_concave
        z_diff = z_diff_concave
        z_diff_rms = z_diff_rms_concave
        shape = "concave"
        pm = 1
    elif z_diff_rms_concave > z_diff_rms_convex:
        z_sph = z_sph_convex
        z_diff = z_diff_convex
        z_diff_rms = z_diff_rms_convex
        shape = "convex"
        pm = -1
    else:
        raise ValueError("Both concave and convex spheres fit equally, check your data!")
    
    # Uncertainty analysis by propagating errors through the fitting sensitivity, details in the analysis file.
    H = chi_squared_Hessian(model_params=[RoC,x0,y0,z0],
                            model_func=model_sphere,
                            model_args={"x":x_flat,"y":y_flat,"shape":shape},
                            data=z_flat,
                            data_unc = z_unc_flat)

    E = np.linalg.inv(H)
    
    #RoC_unc_uncorr = 1/np.sqrt(np.abs(H[0,0]))
    #x0_unc_uncorr = 1/np.sqrt(np.abs(H[1,1]))
    #y0_unc_uncorr = 1/np.sqrt(np.abs(H[2,2]))
    #z0_unc_uncorr = 1/np.sqrt(np.abs(H[3,3]))
    
    # Taken from the chi squared statistic for p-value of 95% confidence (2sigma) with v=4 fitted parameters.
    # For further details see Numerical Recipes p.815.
    delta_chi_sq_nu = 9.72 
    # Fully correlated errors, assuming normal distribution
    RoC_unc_corr = np.sqrt(np.abs(E[0,0])) * np.sqrt(delta_chi_sq_nu)
    x0_unc_corr = np.sqrt(np.abs(E[1,1])) * np.sqrt(delta_chi_sq_nu)
    y0_unc_corr = np.sqrt(np.abs(E[2,2])) * np.sqrt(delta_chi_sq_nu)
    z0_unc_corr = np.sqrt(np.abs(E[3,3])) * np.sqrt(delta_chi_sq_nu)

    return {"RoC" : RoC*pm,
            "H" : H,
            "E" : E,
            "shape" : shape,
            "RoC_unc_corr" : RoC_unc_corr,
            "x0" : x0,
            "x0_unc_corr" : x0_unc_corr,
            "y0" : y0,
            "y0_unc_corr" : y0_unc_corr,
            "z0" : z0,
            "z0_unc_corr" : z0_unc_corr,
            "R_sq" : R_sq,
            "R_sq_adj" : R_sq_adj,
            "x_flat" : x_flat,
            "y_flat" : y_flat,
            "z_flat" : z_flat,
            "z_sph" : z_sph,
            "z_diff" : z_diff,
            "z_diff_rms" : z_diff_rms}


def fit_paraboloid(r,z):
    if hasattr(r,"mask"):
        if hasattr(z,"mask"):
            r_flat = r.compressed()
            z_flat = z.compressed()
        else:
            raise TypeError("r,z must be consistently masked! Check your data.")
    else:
        if hasattr(z,"mask"):
            raise TypeError("x,y,z must be consistently masked! Check your data.")
        else:
            r_flat = r.flatten()
            z_flat = z.flatten()

    # Assemble the A matrix
    A = np.zeros((len(r_flat),1))
    A[:,0] = r_flat**2
    
    # Assemble the B matrix
    B = np.zeros((len(r_flat),1))
    B[:,0] = z_flat
    
    # Call linear least squares
    C, residuals, rank, singval = np.linalg.lstsq(A,B,rcond=None)
    # Calculating resulting parameters
    f = 1/4/C[0][0]

    DoF = len(r_flat)-3-1
    R_sq = 1 - residuals[0]/np.sum((B-np.mean(B))**2)
    R_sq_adj = 1-(1-R_sq)*((len(r_flat)-1)/DoF)
    
    z_parab = model_paraboloid(params=[f], args={"r":r})
    
    return {"f":f,
            "z_parab" : z_parab,
            "R_sq" : R_sq,
            "R_sq_adj" : R_sq_adj
            }


#%% Testing

Bo = 0
EigenOrder_r = 10
EigenOrder_theta = 4
EigenVals_SearchRange = (1,1000,0.1)

r_v = np.linspace(0,1,201)
theta_v = np.linspace(0,2*np.pi,181)
r,theta = np.meshgrid(r_v,theta_v)
r = np.ma.MaskedArray(r,mask=np.full_like(r,False))
r.mask[5,5] = True
theta = np.ma.MaskedArray(theta,mask=r.mask)

EigenVals = calc_LiquidDynamics_EigenVals(EigenOrder_r=EigenOrder_r,EigenOrder_theta=EigenOrder_theta,Bo=Bo,search_range=EigenVals_SearchRange)
alpha_nm,beta_nm = calc_LiquidDynamics_SpatialEigenVals(x=EigenVals,Bo=Bo)
EigenFuncs = calc_LiquidDynamics_EigenFuncs(r=r,theta=theta,EigenVals=EigenVals,Bo=Bo)

# Test case 0
z0 = (1-r**2)
test0 = z0 - EigenFuncs[0][0]
res0 = fit_LiquidDynamics_EigenFuncs(z0,EigenFuncs)
AnBn0 = res0["AnBn"]

# Test case 1
i = 2
j = 1
f1 = sp.special.jv(i,alpha_nm[i,j]*r)-(sp.special.jv(i,alpha_nm[i,j])/sp.special.ive(i,beta_nm[i,j]))*sp.special.ive(i,beta_nm[i,j]*r)*np.exp(beta_nm[i,j]*(r-1))
z1 = f1 * np.sin(i*theta)
test1 = z1 - EigenFuncs[2*i+1][j+1]
res1 = fit_LiquidDynamics_EigenFuncs(z1,EigenFuncs)
AnBn1 = res1["AnBn"]

# Test case 2
i = 0
j = 4
f2 = sp.special.jv(i,alpha_nm[i,j]*r)-(sp.special.jv(i,alpha_nm[i,j])/sp.special.ive(i,beta_nm[i,j]))*sp.special.ive(i,beta_nm[i,j]*r)*np.exp(beta_nm[i,j]*(r-1))
z2 = f2 * np.cos(i*theta)
test2 = z2 - EigenFuncs[i][j+1]
res2 = fit_LiquidDynamics_EigenFuncs(z2,EigenFuncs)
AnBn2 = res2["AnBn"]

# Test case 3
f3 = np.zeros_like(r)
f3[0,0] = 1e6
z3 = f3
res3 = fit_LiquidDynamics_EigenFuncs(z3,EigenFuncs)
AnBn3 = res3["AnBn"]
