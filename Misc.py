# -*- coding: utf-8 -*-
"""
@ Omer Luria
Fluidic Technologies Laboratory
Technion - Israel Institute of Technology
luriomer@gmail.com , lomer@technion.ac.il

A set of miscellaneous tools.
"""
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
from mpl_toolkits import axes_grid1
import cv2

#%%
def interpolate_nans(data_array, pkind='linear'):
    """Interpolates data to fill nan values. 
        Taken from https://stackoverflow.com/questions/6518811/interpolate-nan-values-in-a-numpy-array"""
    aindexes = np.arange(data_array.shape[0])
    agood_indexes, = np.where(np.isfinite(data_array))
    f_good = interp1d(agood_indexes
               , data_array[agood_indexes]
               , bounds_error=False
               , copy=False
               , fill_value="extrapolate"
               , kind=pkind)
    return f_good(aindexes)

#%%
def add_colorbar(im, aspect=20, pad_fraction=0.5, **kwargs):
    """Add a vertical color bar to an image plot, 
       Taken from https://stackoverflow.com/questions/18195758/set-matplotlib-colorbar-size-to-match-graph (Matthias) """
    divider = axes_grid1.make_axes_locatable(im.axes)
    width = axes_grid1.axes_size.AxesY(im.axes, aspect=1./aspect)
    pad = axes_grid1.axes_size.Fraction(pad_fraction, width)
    current_ax = plt.gca()
    cax = divider.append_axes("right", size=width, pad=pad)
    plt.sca(current_ax)
    return im.axes.figure.colorbar(im, cax=cax, **kwargs)


#%%
def gradient_with_masked(z,edge_order=1):
    ''' Calculates the gradient of a 2D masked array, by iterating through the rows and columns (sequentially)
        and calculating 1D gradients only on the unmasked slices.'''
    gradx = np.zeros_like(z)
    grady = np.zeros_like(z)
    xmaskslices = np.ma.notmasked_contiguous(z,axis=1)
    ymaskslices = np.ma.notmasked_contiguous(z,axis=0)
    for i in range(np.shape(z)[0]):
        if xmaskslices[i] == []:
            gradx.mask[i,:] = True
        else:
            for xslice in xmaskslices[i]:
                if xslice.stop-xslice.start <= edge_order+1:
                    gradx.mask[i,xslice] = True
                    grady.mask[i,xslice] = True
                else:
                    gradx[i,xslice] = np.gradient(z[i,xslice],edge_order=edge_order)
    for j in range(np.shape(z)[1]):
        if ymaskslices[j] == []:
            grady.mask[:,j] = True
        else:
            for yslice in ymaskslices[j]:
                if yslice.stop-yslice.start <= edge_order+1:
                    gradx.mask[yslice,j] = True
                    grady.mask[yslice,j] = True
                else:
                    grady[yslice,j] = np.gradient(z[yslice,j],edge_order=edge_order)
    
    return [gradx,grady]

#%%
def get_zenith_angles(z,dx,dy,edge_order=1):
    ''' Receives a surface z(x,y) and the lateral differences scalars dx,dy (all must be the same units) 
        Returns the zenith angles in radians.
        z must be a 2D array, the returned value has the same shape.'''
        
    # Call our special gradient function that would account for the mask
    gradx,grady = gradient_with_masked(z,edge_order)
    # Rescale gradx and grady to account for the lateral dimensions - divide by dx and dy
    gradx /= dx
    grady /= dy
    zenith = np.arccos(1/np.sqrt(gradx**2+grady**2+1))
    return zenith

def get_lateral_displacements(z,z_H,dx,dy,beta,edge_order=1):
    ''' Received the surface coordinates and zenith angles, and returns the xy displacements due to the ray angles of incidence.
        z(x,y) is the surface
        dx and dy are the lateral differences in the xy grid.
        z_H is the height of the baseline surface
        everything must have the same units. '''
    
    # Call our special gradient function that would account for the mask
    gradx,grady = gradient_with_masked(z,edge_order)
    # Rescale gradx and grady to account for the lateral dimensions - divide by dx and dy
    gradx /= dx
    grady /= dy
    
    zeta = np.arctan2(grady,gradx)
    
    disp_x = np.cos(zeta) * np.tan(beta) * (z_H-z)
    disp_y = np.sin(zeta) * np.tan(beta) * (z_H-z)
    
    return [disp_x,disp_y]
    
#%% ctypes MessageBoxW parameters
''' ctype windll messagebox buttons. See also 
https://stackoverflow.com/questions/27257018/python-messagebox-with-icons-using-ctypes-and-windll

https://learn.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-messagebox?redirectedfrom=MSDN

https://stackoverflow.com/questions/34840838/how-to-specify-what-actually-happens-when-yes-no-is-clicked-with-ctypes-messageb
'''

# Format: ctypes.windll.user32.MessageBoxW(None, "text", "caption", MB_X | ICON_X)

# Window format and behavior
MB_OK = 0
MB_OKCANCEL = 1
MB_YESNOCANCEL = 3
MB_YESNO = 4

# Returned values
IDOK = 1
IDCANCEL = 2
IDABORT = 3
IDYES = 6
IDNO = 7

# icons
ICON_EXCLAIM = 48
ICON_INFO = 64
ICON_QUESTION = 32
ICON_STOP = 16

#%% Finding the smallest circle which contains all False elemets in a boolean array.

def all_values_contained(x0,y0,R,boolean_array,x_coord,y_coord):
    ''' The discrete cost function. If the circle does not encloses all the False calues, it returns None.
        Otherwise, it returns the total number of True elements enclosed by that circle in the binary array.
        We use  x_coord and y_coord to correctly interpret the distances on the array.
        As long as the radius equals or larger than the eucliean distance between the points, there are two solutions, so we also specify which sign to take. '''
    
    r_coord = np.hypot(x_coord-x0,y_coord-y0)
    
    masked_boolean_array = np.ma.MaskedArray(boolean_array,mask=False)
    masked_boolean_array.mask[r_coord>R] = True
    
    total_false_count = np.sum(~boolean_array)
    unmasked_false_count = np.sum(~masked_boolean_array)
    # If not all False are contained within the circle, return None
    if total_false_count == unmasked_false_count:
        return True
    else:
        return False
    
    
def find_smallest_circle(boolean_array,x_coord,y_coord,threshold=1e-2):
    ''' Take a boolean array, coordinates array and scale, and finds the smallest circle
        which encircles all "False" values. '''
    Renc = np.zeros_like(x_coord)
    imin = 0
    jmin = 0
    for i in range(len(x_coord[0,:])):
        for j in range(len(y_coord[:,0])):
            # We use the bisection method to iteratively change radii values until we converge.
            Rlow = 0
            Rhigh = np.hypot(np.ptp(x_coord),np.ptp(y_coord))
            while ((Rhigh-Rlow)/Rhigh > threshold):
                Rmid = (Rhigh + Rlow) / 2
                condition = all_values_contained(x_coord[0,i],y_coord[j,0],Rmid,boolean_array,x_coord,y_coord)
                if condition:
                    Rhigh = Rmid
                else:
                    Rlow = Rmid
            # After converging, we take Rhigh to make sure there won't be a single pixel left our of the final radii.
            # If we take Rmid we might miss pixels at the edges. So effectively we take the smallest one that for sure encloses everything.
            Renc[j,i] = Rhigh
            if Rhigh < Renc[jmin,imin]:
                imin = i
                jmin = j
    return (x_coord[0,imin],y_coord[jmin,0]),Renc[jmin,imin]