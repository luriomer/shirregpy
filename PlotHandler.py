# -*- coding: utf-8 -*-
"""
@ Omer Luria
Fluidic Technologies Laboratory
Technion - Israel Institute of Technology
luriomer@gmail.com , lomer@technion.ac.il

This code handles all the plotting functions.
"""

import numpy as np
from scipy.fftpack import fftshift
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
from matplotlib.colors import LogNorm
from matplotlib.ticker import MaxNLocator
import warnings
import Misc

def cursor_format(current, other):
    ''' Cursor format with two x axes. Based on:
        https://stackoverflow.com/questions/21583965/matplotlib-cursor-value-with-two-axes (unutbu) '''
    # current and other are axes
    def format_coord(x, y):
        # x, y are data coordinates
        # convert to display coords
        display_coord = current.transData.transform((x,y))
        inv = other.transData.inverted()
        # convert back to data coords with respect to ax
        ax_coord = inv.transform(display_coord)
        coords = [(x, y) , ax_coord]
        cursor_str = "Time "+"{:.2f}".format(coords[0][0])+" s ; Frame "+"{:.0f}".format(coords[1][0])+" ; Value "+"{:.3f}".format(coords[0][1],2)
        return cursor_str
    return format_coord

class PlotHandler:
    ''' A class for handling plots '''
    def __init__(self,Data,Recon,fontsize=12):
        self.Data = Data
        self.Recon = Recon
        self.fontsize = fontsize

    
    def plot_Env_data(self,fig,i):
        self.EnvAx1 = fig.add_subplot(311)
        self.EnvAx1.plot(self.Data.time,self.Data.AccInt_res,marker='.',alpha=0.5,linestyle='--',color='blue',
                                label="Internal IMU ("+"{:.3f}".format(self.Data.AccInt_res[i])+" g)")
        self.EnvAx1.plot(self.Data.time,self.Data.AccExt_res_filt,marker='.',alpha=0.5,linestyle='--',color='green',
                                label="External IMU ("+"{:.3f}".format(self.Data.AccExt_res_filt[i])+" g)")
        self.EnvTimeline1 = self.EnvAx1.axvline(x=self.Data.time[i],color="black",linestyle='-.',
                                 label="Timeline (Frame "+str(i)+", Time "+"{:.3f}".format(self.Data.time[i])+" sec)")
        self.EnvAx1.set_ylabel("Resultant proper acceleration [g]",fontsize=self.fontsize)
        self.EnvAx1.set_xlim(np.min(self.Data.time),np.max(self.Data.time))
        self.EnvAx1.set_ylim(-0.1,2.2)
        self.EnvAx1_legend = self.EnvAx1.legend(loc='upper right')
        self.EnvAx1.tick_params(axis='x', which='both', bottom=True, top=False, labelbottom=False, labeltop=False)
        self.EnvAx1_sec = self.EnvAx1.secondary_xaxis('top', functions=(self.Data.time_to_frame, self.Data.frame_to_time))
        self.EnvAx1_sec.xaxis.set_major_locator(MaxNLocator(integer=True))
        self.EnvAx1_sec.set_xlim(0,self.Data.frame_count-1)
        self.EnvAx1.format_coord = cursor_format(self.EnvAx1, self.EnvAx1_sec)
        self.EnvAx1_sec.set_xlabel("Frame #",fontsize=self.fontsize)
        self.EnvAx1_sec.tick_params(axis='x', which='both', bottom=False, top=True, labelbottom=False, labeltop=True)
        
        self.EnvAx2 = fig.add_subplot(312, sharex = self.EnvAx1)
        self.EnvAx2.plot(self.Data.time,self.Data.PressExt_filt/1e3,marker='.',linestyle='--')
        self.EnvTimeline2 = self.EnvAx2.axvline(x=self.Data.time[i],color="black",linestyle='-.',
                                 label="Timeline (Frame "+str(i)+", Time "+"{:.3f}".format(self.Data.time[i])+" sec)")
        self.EnvAx2.set_ylabel("Cabin air pressure [kPa]",fontsize=self.fontsize)
        self.EnvAx2.tick_params(axis='x', which='both', bottom=True, top=False, labelbottom=False, labeltop=False)
        self.EnvAx2_sec = self.EnvAx2.secondary_xaxis('top', functions=(self.Data.time_to_frame, self.Data.frame_to_time))
        self.EnvAx2_sec.xaxis.set_major_locator(MaxNLocator(integer=True))
        self.EnvAx2_sec.set_xlim(0,self.Data.frame_count-1)
        self.EnvAx2.format_coord = cursor_format(self.EnvAx2, self.EnvAx2_sec)
        self.EnvAx2_sec.tick_params(axis='x', which='both', bottom=False, top=True, labelbottom=False, labeltop=False)
        
        self.EnvAx3 = fig.add_subplot(313, sharex = self.EnvAx1)
        self.EnvAx3.plot(self.Data.time,self.Data.PumpAction,label="Pump action (0 - Standby, 1- Running)")
        self.EnvAx3.plot(self.Data.time,self.Data.PumpDirection,label="Pump direction (0 - Backward, 1 - Forward)")
        #self.EnvAx3.plot(self.Data.time,self.Data.PumpMode,label="Pump mode (0 - Manual, 1 - Auto)")
        #self.EnvAx3.plot(self.Data.time,self.Data.SyncButtonLabView,label="User button")
        self.EnvTimeline3 = self.EnvAx3.axvline(x=self.Data.time[i],color="black",linestyle='-.',
                                 label="Timeline (Frame "+str(i)+", Time "+"{:.3f}".format(self.Data.time[i])+" sec)")
        self.EnvAx3.set_ylabel("I/O events [a.u.]",fontsize=self.fontsize)
        self.EnvAx3.set_xlabel("Time [s]",fontsize=self.fontsize)
        self.EnvAx3.legend()
        self.EnvAx3.tick_params(axis='x', which='both', bottom=True, top=False, labelbottom=True, labeltop=False)
        self.EnvAx3_sec = self.EnvAx3.secondary_xaxis('top', functions=(self.Data.time_to_frame, self.Data.frame_to_time))
        self.EnvAx3_sec.xaxis.set_major_locator(MaxNLocator(integer=True))
        self.EnvAx3_sec.set_xlim(0,self.Data.frame_count-1)
        self.EnvAx3.format_coord = cursor_format(self.EnvAx3, self.EnvAx3_sec)
        self.EnvAx3_sec.tick_params(axis='x', which='both', bottom=False, top=True, labelbottom=False, labeltop=False)


    def update_Env_plot(self,i):
        self.EnvTimeline1.set_xdata(x=self.Data.time[i])
        self.EnvTimeline2.set_xdata(x=self.Data.time[i])
        self.EnvTimeline3.set_xdata(x=self.Data.time[i])
        self.EnvAx1_legend.get_texts()[0].set_text("Internal IMU ("+"{:.3f}".format(self.Data.AccInt_res[i])+" g)")
        self.EnvAx1_legend.get_texts()[1].set_text("External IMU ("+"{:.3f}".format(self.Data.AccExt_res_filt[i])+" g)")
        self.EnvAx1_legend.get_texts()[2].set_text("Timeline (Frame "+str(i)+", Time "+"{:.3f}".format(self.Data.time[i])+" sec)")


    def plot_WF(self,fig,subplot,cmap="jet"):
        self.WFAx = fig.add_subplot(subplot)
        self.WFMask = self.WFAx.contour(self.Data.PupilMask[self.Data.BootFrame],linestyles='--',colors='black',
                                        extent=[-self.Data.x_mm/2,self.Data.x_mm/2,-self.Data.y_mm/2,self.Data.y_mm/2])
        self.WFImg = self.WFAx.imshow(self.Data.WF[self.Data.BootFrame].data,cmap=cmap,origin="lower",rasterized=True,
                                           extent=[-self.Data.x_mm/2,self.Data.x_mm/2,-self.Data.y_mm/2,self.Data.y_mm/2])
        self.WFcbar = Misc.add_colorbar(self.WFImg)
        self.pupilCirc = plt.Circle((self.Data.PupilXCenter[self.Data.BootFrame],
                                     self.Data.PupilYCenter[self.Data.BootFrame]),
                                    self.Data.PupilDiameter[self.Data.BootFrame]/2,
                                    fill=False,edgecolor="gray",linestyle='-.',linewidth=1.5)
        plt.gca().add_artist(self.pupilCirc)
        self.WF_CenterMark = self.WFAx.scatter(self.Data.x0_WF[self.Data.BootFrame],self.Data.y0_WF[self.Data.BootFrame],marker='+',s=1500,linewidths=1.5,color='black')
        self.WFcbar.ax.set_title("$\mu$m", fontsize=self.fontsize)
        self.WFAx.set_title("Measured wavefront (SH plane) "+self.Data.time_RTC[self.Data.BootFrame][:8]+"\n"
                                 #+"RoC "+'{:.1f}'.format(self.Data.RoC_WF[self.Data.BootFrame])+", "
                                 +"f$_{MUT}$ "+'{:.2f}'.format(self.Data.f_MUT[self.Data.BootFrame])+"$\pm$"+'{:.2f}'.format(np.abs(self.Data.f_MUT_unc[self.Data.BootFrame]))+", "
                                 +"d$_{MUT}$ "+'{:.2f}'.format(self.Data.d_MUT[self.Data.BootFrame])+" mm")
        self.WFAx.set_xlabel("x [mm]",fontsize=self.fontsize)
        self.WFAx.set_ylabel("y [mm]",fontsize=self.fontsize)
        self.WFAx.set_aspect(1)
        fig.tight_layout()
        
        
    def update_WFplot(self,i):
        for coll in self.WFMask.collections: 
            coll.remove() 
        self.WFMask = self.WFAx.contour(self.Data.PupilMask[i],linestyles='--',colors='black',
                                        extent=[-self.Data.x_mm/2,self.Data.x_mm/2,-self.Data.y_mm/2,self.Data.y_mm/2])
        self.WFImg.set_data(self.Data.WF[i].data)
        self.WFImg.set_clim(np.nanmin(self.Data.WF[i].data),np.nanmax(self.Data.WF[i].data))
        self.WF_CenterMark.set_offsets([self.Data.x0_WF[i],self.Data.y0_WF[i]])
        self.pupilCirc.set(radius=self.Data.PupilDiameter[i]/2,center=(self.Data.PupilXCenter[i],self.Data.PupilYCenter[i])) 
        self.WFAx.set_title("Measured wavefront (SH plane) "+self.Data.time_RTC[i][:8]+"\n"
                                 #+"RoC "+'{:.1f}'.format(self.Data.RoC_WF[i])
                                 +"f$_{MUT}$ "+'{:.2f}'.format(self.Data.f_MUT[i])+"$\pm$"+'{:.2f}'.format(np.abs(self.Data.f_MUT_unc[i]))+", "
                                 +"d$_{MUT}$ "+'{:.2f}'.format(self.Data.d_MUT[i])+" mm")
        
        
    def plot_Zernikes(self,fig,subplot,**kwargs):
        self.ZerSubplot = fig.add_subplot(subplot)
        if len(self.Data.ZerNames)<self.Data.MaxZerOrder:
            self.ZerNames = self.Data.ZerNames + [""]*(len(self.Data.Zc[self.Data.BootFrame])-len(self.Data.ZerNames))
        self.ZerPlot = self.ZerSubplot.bar(self.Data.ZerNames[:self.Data.MaxZerOrder],self.Data.Zc[self.Data.BootFrame][:self.Data.MaxZerOrder],color="black")
        self.ZerSubplot.set_title("Zernike decomposition\n$R^2$ = "+'{:.3f}'.format(self.Data.ZRsq[self.Data.BootFrame])+r", $\bar{R}^2$ = "+'{:.3f}'.format(self.Data.ZRsq_adj[self.Data.BootFrame]))
        plt.xticks(rotation=45,ha="right")
        plt.xlim(-1,self.Data.MaxZerOrder)
        self.ZerSubplot.set_ylabel("Zernike coefficient [$\mu$m]",fontsize=self.fontsize)
        #self.ZerSubplot.grid(axis="y")
        minrange = np.min(self.Data.Zc[self.Data.BootFrame][:self.Data.MaxZerOrder])
        maxrange = np.max(self.Data.Zc[self.Data.BootFrame][:self.Data.MaxZerOrder])
        diffrange = maxrange-minrange
        self.ZerSubplot.set_ylim(minrange-diffrange*0.05,maxrange+diffrange*0.05)
        self.ZerSubplot.set_yscale('symlog',linthresh=np.min(np.abs(self.Data.Zc[self.Data.BootFrame])))
        self.ZerPlotCenterTextBox = self.ZerSubplot.text(17,0,"Processing failed",fontsize=36,color="red",backgroundcolor="white",horizontalalignment='center',
verticalalignment='center',visible=False)
        fig.tight_layout()
        
        
    def update_Zernikes(self,i):
        if self.Data.processing_success[i]:
            for j in range(len(self.ZerPlot)):
                self.ZerPlot[j].set_height(self.Data.Zc[i][j])
            self.ZerSubplot.relim()
            self.ZerSubplot.autoscale_view()
            self.ZerSubplot.set_title("Zernike decomposition\n$R^2$ = "+'{:.3f}'.format(self.Data.ZRsq[i])+r", $\bar{R}^2$ = "+'{:.3f}'.format(self.Data.ZRsq_adj[i]))
            minrange = np.min(self.Data.Zc[i][:self.Data.MaxZerOrder])
            maxrange = np.max(self.Data.Zc[i][:self.Data.MaxZerOrder])
            diffrange = maxrange-minrange
            self.ZerSubplot.set_ylim(minrange-diffrange*0.05,maxrange+diffrange*0.05)
            self.ZerSubplot.set_yscale('symlog',linthresh=np.min(np.abs(self.Data.Zc[i])))
            self.ZerPlotCenterTextBox.set_visible(False)
        else:
            for j in range(len(self.ZerPlot)):
                self.ZerPlot[j].set_height(0)
            self.ZerSubplot.set_title("Zernike decomposition")
            self.ZerSubplot.set_ylim(-1,1)
            self.ZerPlotCenterTextBox.set_visible(True)


    def plot_WFE(self,fig,subplot,**kwargs):
        ''' Plots the Reconstructed wavefront error '''
        for kwarg in kwargs:
            if kwarg not in ["cmap","interp_method","show_pupil","show_legend"]:
                warnings.warn("plot_PSF: kwarg "+kwarg+" not recognized.\n")
        cmap = kwargs.setdefault("cmap","jet")
        interp_method = kwargs.setdefault("interp_method","bicubic")
        show_legend = kwargs.setdefault("show_legend",False)
        self.WFEAx = fig.add_subplot(subplot)
        self.WFEAx.set_title("Wavefront aberration (MUT plane)\nRMS "+'{:.2f}'.format(self.Recon.WFE_RMS)+", P-V "+'{:.2f}'.format(self.Recon.WFE_P2V))
        self.WFEImg = plt.imshow(self.Recon.WFE,cmap=cmap,interpolation=interp_method,aspect="equal",origin="lower",
                   extent=[-self.Recon.x_mm/2, self.Recon.x_mm/2, -self.Recon.y_mm/2, self.Recon.y_mm/2], rasterized=True)
        self.WFEImg.set_clim(np.min(self.Recon.WFE),np.max(self.Recon.WFE))
        self.WFE_CenterMark = self.WFEAx.scatter(self.Data.x0_WF[self.Data.BootFrame]*self.Data.conj_mag[self.Data.BootFrame],self.Data.y0_WF[self.Data.BootFrame]*self.Data.conj_mag[self.Data.BootFrame],marker='+',s=1500,linewidths=1.5,color='black')
        self.WFEcbar = Misc.add_colorbar(self.WFEImg)
        self.WFEcbar.ax.set_title("$\mu$m", fontsize=self.fontsize)
        self.WFEBlack_line = mlines.Line2D([], [], color='black', linestyle='--')
        if show_legend:
            plt.legend([self.WFEBlack_line],["Pupil"])
        plt.xlabel("x [mm]",fontsize=self.fontsize)
        plt.ylabel("y [mm]",fontsize=self.fontsize)
        fig.tight_layout()
        
    
    def update_WFE_plot(self,i):
        self.WFEImg.set_data(self.Recon.WFE)
        self.WFE_CenterMark.set_offsets([self.Data.x0_WF[i]*self.Data.conj_mag[i],self.Data.y0_WF[i]*self.Data.conj_mag[i]])
        self.WFEImg.set_clim(np.min(self.Recon.WFE),np.max(self.Recon.WFE))
        self.WFEAx.set_title("Wavefront aberration (MUT plane)\nRMS "+'{:.2f}'.format(self.Recon.WFE_RMS)+", P-V "+'{:.2f}'.format(self.Recon.WFE_P2V))
        
        
    def plot_PSF(self,fig,subplot,**kwargs):
        ''' Plot the PSF '''
        for kwarg in kwargs:
            if kwarg not in ["scale","vmin","cmap","interp_method","show_Airy_disk","show_grid","show_center_cross"]:
                warnings.warn("plot_PSF: kwarg "+kwarg+" not recognized.\n")
        scale = kwargs.setdefault("scale","linear")
        vmin = kwargs.setdefault("vmin",1e-3)
        cmap = kwargs.setdefault("cmap","gray")
        interp_method = kwargs.setdefault("interp_method","bicubic")
        show_Airy_disk = kwargs.setdefault("show_Airy_disk",True)
        show_grid = kwargs.setdefault("show_grid",False)
        show_center_cross = kwargs.setdefault("show_center_cross",False)
        self.PSFAx = fig.add_subplot(subplot)
        plt.title("Simulated PSF")
        if scale == "log":
            norm = norm=LogNorm(vmin=vmin, vmax=1)
        elif scale == "linear":
            norm = None
        if self.Recon.rigorous_response_analysis:
            self.PSFImg = plt.imshow(self.Recon.PSF,norm = norm,cmap=cmap,interpolation=interp_method,origin="lower",
                       aspect="equal",extent=[np.min(self.Recon.ax_img), np.max(self.Recon.ax_img), np.min(self.Recon.ax_img), np.max(self.Recon.ax_img)], rasterized=True)
            PSFcb = Misc.add_colorbar(self.PSFImg)
            PSFcb.ax.set_title('a.u.', fontsize=self.fontsize)
            if show_Airy_disk:
                self.AiryCirc = plt.Circle((0,0),self.Recon.AiryDisk_angle,fill=False,edgecolor="red",linewidth=1.5,linestyle='--')
                plt.gca().add_artist(self.AiryCirc)
            if show_center_cross:
                plt.plot(0,0,marker='+',markersize=20,color='black')
            if show_grid:
                plt.grid()
            self.Red_line = mlines.Line2D([], [], color='red', linestyle='--')
            plt.legend([self.Red_line],["Airy disk"],framealpha=0.5)
            plt.xlim(np.min(self.Recon.ax_img),np.max(self.Recon.ax_img))
            plt.ylim(np.min(self.Recon.ax_img),np.max(self.Recon.ax_img))
        self.PSFAx.set_aspect('equal', 'box')
        plt.xlabel(r"$\theta_x$ [mrad]",fontsize=self.fontsize)
        plt.ylabel(r"$\theta_y$ [mrad]",fontsize=self.fontsize)
        fig.tight_layout()
        
    
    def update_PSF_plot(self):
        if self.Recon.rigorous_response_analysis:
            self.PSFImg.set_data(self.Recon.PSF)
            self.PSFImg.set_extent([np.min(self.Recon.ax_img), np.max(self.Recon.ax_img), np.min(self.Recon.ax_img), np.max(self.Recon.ax_img)])
            self.PSFImg.set_clim(np.min(self.Recon.PSF),np.max(self.Recon.PSF))
            self.AiryCirc.set_radius(self.Recon.AiryDisk_angle)
            self.PSFAx.set_xlim(np.min(self.Recon.ax_img),np.max(self.Recon.ax_img))
            self.PSFAx.set_ylim(np.min(self.Recon.ax_img),np.max(self.Recon.ax_img))
            
            
        def plot_EE(self,fig,subplot,**kwargs):
            ''' Plots the encircled energy '''
            for kwarg in kwargs:
                if kwarg not in ["threshold","maxlimit"]:
                    warnings.warn("plot_EE: kwarg "+kwarg+" not recognized.\n")
            maxlimit = kwargs.setdefault("maxlimit",0.99)
            if maxlimit < 0.0 or maxlimit >1.0:
                warnings.warn("plot_EE: maxlimit must be between 0 and 1. Resetting to default.\n")
            EEthreshold = kwargs.setdefault("threshold",0.838)
            if EEthreshold < 0.0 or EEthreshold >1.0:
                warnings.warn("plot_EE: EEthreshold must be between 0 and 1. Resetting to default.\n")
            EEplot = fig.add_subplot(subplot)
            EEplot.plot(self.Recon.rEE,self.Recon.EE,color="black")
            EEplot.set_xlabel("r [mrad]",fontsize=self.fontsize)
            EEplot.set_ylabel("Encircled Energy [a.u.]",fontsize=self.fontsize)
            EEplot.set_xlim(-0.05*self.Recon.rEE_interp(maxlimit),self.Recon.rEE_interp(maxlimit))
            EEplot.plot(self.Recon.rEE_interp(EEthreshold),EEthreshold,marker='o',markersize=5.0,color="blue")
            EEplot.text(self.Recon.rEE_interp(EEthreshold)*0.5,EEthreshold+0.02,"("+'{:.3f}'.format(float(self.Recon.rEE_interp(EEthreshold)))+","+str(EEthreshold)+")",color="blue",fontsize=self.fontsize)
            EEplot.grid()
        
        
    def plot_MTF(self,fig,subplot,**kwargs):
        ''' Plot the MTF '''
        for kwarg in kwargs:
            if kwarg not in ["scale","cmap","interp_method"]:
                warnings.warn("plot_MTF: kwarg "+kwarg+" not recognized.\n")
        scale = kwargs.setdefault("scale","linear")
        cmap = kwargs.setdefault("cmap","plasma")
        interp_method = kwargs.setdefault("interp_method","bicubic")
        #show_diff_limit = kwargs.setdefault("show_diff_limit",False)
        
        self.MTFAx = fig.add_subplot(subplot)
        plt.title("Simulated MTF")
        if scale == "log":
            norm = LogNorm(vmin=1e-2, vmax=1)
        elif scale == "linear":
            norm = None
        if self.Recon.rigorous_response_analysis:
            self.MTFImg = self.MTFAx.imshow(fftshift(self.Recon.MTF),norm=norm,cmap=cmap,interpolation=interp_method,aspect="equal",origin="lower",
                       extent=[np.min(self.Recon.nu_img), np.max(self.Recon.nu_img), np.min(self.Recon.nu_img), np.max(self.Recon.nu_img)], rasterized=True)
            self.MTFcb = Misc.add_colorbar(self.MTFImg)
            self.MTFcb.ax.set_title('a.u.',fontsize=self.fontsize)
            #if show_diff_limit:
            #    self.ResCirc = plt.Circle((0,0),self.Recon.nu_Airy,fill=False,edgecolor="red",linestyle='--',linewidth=1.5)
            #    plt.gca().add_artist(self.ResCirc)       
            self.MTFLimitContour = self.MTFAx.contour(fftshift(self.Recon.MTF),levels=[0.1],origin="lower",colors=["black"],linewidths=[1.5],linestyles="--",
                            extent=[np.min(self.Recon.nu_img), np.max(self.Recon.nu_img), np.min(self.Recon.nu_img), np.max(self.Recon.nu_img)])
            #Red_line = mlines.Line2D([], [], color='red', linestyle='--')
            self.MTFBlack_line = mlines.Line2D([], [], color='black', linestyle='--')
            #plt.legend([Red_line,self.MTFBlack_line],["Diffraction limit","10% limit"],framealpha=0.5)
            plt.legend([self.MTFBlack_line],["10% limit"],framealpha=0.5)
            self.MTFAx.set_xlim(0,np.max(self.Recon.nu_img))
            self.MTFAx.set_ylim(0,np.max(self.Recon.nu_img))
        self.MTFAx.set_aspect('equal', 'box') 
        plt.xlabel(r"$\nu_x$ [mrad$^{-1}$]",fontsize=self.fontsize)
        plt.ylabel(r"$\nu_y$ [mrad$^{-1}$]",fontsize=self.fontsize)
        
        fig.tight_layout()
        
    
    def update_MTF_plot(self):
        if self.Recon.rigorous_response_analysis:
            for coll in self.MTFLimitContour.collections:
                coll.remove()
            self.MTFLimitContour = self.MTFAx.contour(fftshift(self.Recon.MTF),levels=[0.1],colors=["black"],linewidths=[1.5],linestyles="--",
                            extent=[np.min(self.Recon.nu_img), np.max(self.Recon.nu_img), np.min(self.Recon.nu_img), np.max(self.Recon.nu_img)])
            self.MTFImg.set_data(fftshift(self.Recon.MTF))
            self.MTFLimitContour.set_linewidth(1.5)
            self.MTFImg.set_clim(np.min(fftshift(self.Recon.MTF)),np.max(fftshift(self.Recon.MTF)))
            self.MTFImg.set_extent([np.min(self.Recon.nu_img), np.max(self.Recon.nu_img), np.min(self.Recon.nu_img), np.max(self.Recon.nu_img)])
            self.MTFAx.set_xlim(0,np.max(self.Recon.nu_img))
            self.MTFAx.set_ylim(0,np.max(self.Recon.nu_img))
        
    
    def plot_recon_surface(self,fig,cmap="jet"):
        self.Surf3DAx = fig.add_subplot(2,3,(1,4), projection='3d')
        self.Surf3DAx.set_title("Surface 3D plot")
        
        # This snippet is used to set the z label always on the left side, no matter how we rotate the plot.
        tmp_planes = self.Surf3DAx.zaxis._PLANES 
        self.Surf3DAx.zaxis._PLANES = ( tmp_planes[2], tmp_planes[3], 
                     tmp_planes[0], tmp_planes[1], 
                     tmp_planes[4], tmp_planes[5])
        init_view = (25, -25)
        self.Surf3DAx.view_init(*init_view)
        self.Surf3Dplot = self.Surf3DAx.plot_surface(self.Recon.x, self.Recon.y, self.Recon.z_surf, color='b',alpha=0.5, rasterized=True, label="Surface")
        self.RefSurf3D = self.Surf3DAx.plot_wireframe(self.Recon.x, self.Recon.y, np.where(self.Recon.surf_sphere_fit_z.mask,np.nan,self.Recon.surf_sphere_fit_z), 
                                                        rstride=1,cstride=1,color="r", alpha=0.5,rasterized=True,
                                                        label="Sphere fit\nRoC = "+'{:.2f}'.format(self.Recon.surf_sphere_fit_RoC)+" $\pm$ "
                                                        +'{:.2f}'.format(self.Recon.surf_sphere_fit_RoC_unc)+" mm\n")
        self.Surf3DAx.set_xlabel("x [mm]")
        self.Surf3DAx.set_ylabel("y [mm]")
        self.Surf3DAx.zaxis.set_rotate_label(False)
        self.Surf3DAx.set_zlabel("z [$\mu$m]",rotation=90)
        self.Surf3DAxLegend = self.Surf3DAx.legend()
        
        self.SurfAx = fig.add_subplot(2,3,2)
        self.SurfAx.set_title("Surface contour")
        self.SurfImg = self.SurfAx.imshow(self.Recon.z_surf,extent=[-self.Recon.x_mm/2, self.Recon.x_mm/2, 
                                                                                           -self.Recon.y_mm/2, self.Recon.y_mm/2],
                                                    origin="lower",aspect="equal",cmap = cmap,norm=None,rasterized=True)
        self.SurfAx.set_xlabel("x [mm]",fontsize=self.fontsize)
        self.SurfAx.set_ylabel("y [mm]",fontsize=self.fontsize)
        self.Surf_CenterMark = self.SurfAx.scatter(self.Data.x0_WF[self.Data.BootFrame]*self.Data.conj_mag[self.Data.BootFrame],self.Data.y0_WF[self.Data.BootFrame]*self.Data.conj_mag[self.Data.BootFrame],marker='+',s=1500,linewidths=1.5,color='black')
        SurfCbar = Misc.add_colorbar(self.SurfImg)
        SurfCbar.ax.set_title("$\mu$m",fontsize=self.fontsize)
        
        self.SurfDiffAx = fig.add_subplot(2,3,5)
        self.SurfDiffImg = self.SurfDiffAx.imshow(self.Recon.surf_sphere_fit_resid,extent=[-self.Recon.x_mm/2, self.Recon.x_mm/2, 
                                                                                           -self.Recon.y_mm/2, self.Recon.y_mm/2],
                                                    origin="lower",aspect="equal",cmap = cmap,norm=None,rasterized=True)
        self.SurfDiffAx.set_title("Sphere fit residual "+'{:.2f}'.format(self.Recon.surf_sphere_fit_resid_rms)+"$\pm$"+'{:.2f}'.format(np.abs(self.Recon.surf_sphere_fit_resid_rms_unc))+" $\mu$m RMS")
        self.SurfDiffAx.set_xlabel("x [mm]",fontsize=self.fontsize)
        self.SurfDiffAx.set_ylabel("y [mm]",fontsize=self.fontsize)
        self.SurfDiff_CenterMark = self.SurfDiffAx.scatter(self.Data.x0_WF[self.Data.BootFrame]*self.Data.conj_mag[self.Data.BootFrame],self.Data.y0_WF[self.Data.BootFrame]*self.Data.conj_mag[self.Data.BootFrame],marker='+',s=1500,linewidths=1.5,color='black')
        SurfDiffCbar = Misc.add_colorbar(self.SurfDiffImg)
        SurfDiffCbar.ax.set_title("$\mu$m",fontsize=self.fontsize)
        
        self.EigenSurfAx = fig.add_subplot(2,3,3)
        self.EigenSurfAx.set_title("Eigenfunctions fit")
        self.EigenSurfImg = self.EigenSurfAx.imshow(self.Recon.surf_EigenFuncs_fit_z,extent=[-self.Recon.x_mm/2, self.Recon.x_mm/2, 
                                                                                           -self.Recon.y_mm/2, self.Recon.y_mm/2],
                                                    origin="lower",aspect="equal",cmap = cmap,norm=None,rasterized=True)
        self.EigenSurfAx.set_xlabel("x [mm]",fontsize=self.fontsize)
        self.EigenSurfAx.set_ylabel("y [mm]",fontsize=self.fontsize)
        self.EigenSurf_CenterMark = self.EigenSurfAx.scatter(self.Data.x0_WF[self.Data.BootFrame]*self.Data.conj_mag[self.Data.BootFrame],self.Data.y0_WF[self.Data.BootFrame]*self.Data.conj_mag[self.Data.BootFrame],marker='+',s=1500,linewidths=1.5,color='black')
        EigenSurfCbar = Misc.add_colorbar(self.EigenSurfImg)
        EigenSurfCbar.ax.set_title("$\mu$m",fontsize=self.fontsize)
        
        
        self.EigenSurfDiffAx = fig.add_subplot(2,3,6)
        self.EigenSurfDiffAx.set_title("Eigenfunctions fit residual "+'{:.2f}'.format(self.Recon.surf_EigenFuncs_fit_z_resid_rms)+"$\pm$"+'{:.2f}'.format(np.abs(self.Recon.surf_sphere_fit_resid_rms_unc))+" $\mu$m RMS")
        self.EigenSurfDiffImg = self.EigenSurfDiffAx.imshow(self.Recon.surf_EigenFuncs_fit_z_resid,extent=[-self.Recon.x_mm/2, self.Recon.x_mm/2, 
                                                                                           -self.Recon.y_mm/2, self.Recon.y_mm/2],
                                                    origin="lower",aspect="equal",cmap = cmap,norm=None,rasterized=True)
        self.EigenSurfDiffAx.set_xlabel("x [mm]",fontsize=self.fontsize)
        self.EigenSurfDiffAx.set_ylabel("y [mm]",fontsize=self.fontsize)
        self.EigenSurfDiff_CenterMark = self.EigenSurfDiffAx.scatter(self.Data.x0_WF[self.Data.BootFrame]*self.Data.conj_mag[self.Data.BootFrame],self.Data.y0_WF[self.Data.BootFrame]*self.Data.conj_mag[self.Data.BootFrame],marker='+',s=1500,linewidths=1.5,color='black')
        EigenSurfDiffCbar = Misc.add_colorbar(self.EigenSurfDiffImg)
        EigenSurfDiffCbar.ax.set_title("$\mu$m",fontsize=self.fontsize)
        
    
    def update_recon_surface(self,i):
        self.Surf3Dplot.remove()
        self.RefSurf3D.remove()
        self.Surf3Dplot = self.Surf3DAx.plot_surface(self.Recon.x, self.Recon.y, self.Recon.z_surf, color='b', alpha=0.5, rasterized=True, label="Surface")
        self.RefSurf3D = self.Surf3DAx.plot_wireframe(self.Recon.x, self.Recon.y, np.where(self.Recon.surf_sphere_fit_z.mask,np.nan,self.Recon.surf_sphere_fit_z),
                                                        rstride=1,cstride=1,color="r", alpha=0.5,rasterized=True,
                                                        label="Sphere fit\nRoC = "
                                                      +'{:.2f}'.format(self.Recon.surf_sphere_fit_RoC)+" $\pm$ "
                                                      +'{:.2f}'.format(self.Recon.surf_sphere_fit_RoC_unc)+" mm")
        self.Surf3DAx.set_xlim(np.min(self.Recon.x),np.max(self.Recon.x))
        self.Surf3DAx.set_ylim(np.min(self.Recon.y),np.max(self.Recon.y))
        self.Surf3DAxLegend.get_texts()[1].set_text("Sphere fit\nRoC = "+'{:.2f}'.format(self.Recon.surf_sphere_fit_RoC)+"$\pm$"
                                                      +'{:.2f}'.format(self.Recon.surf_sphere_fit_RoC_unc)+" mm")
        
        self.SurfImg.set_data(self.Recon.z_surf)
        self.SurfImg.set_clim(np.min(self.Recon.z_surf),np.max(self.Recon.z_surf))
        self.Surf_CenterMark.set_offsets([self.Data.x0_WF[i]*self.Data.conj_mag[i],self.Data.y0_WF[i]*self.Data.conj_mag[i]])
        
        self.SurfDiffImg.set_data(self.Recon.surf_sphere_fit_resid)
        self.SurfDiffImg.set_clim(np.min(self.Recon.surf_sphere_fit_resid),np.max(self.Recon.surf_sphere_fit_resid))
        self.SurfDiffAx.set_title("Sphere fit residual "+'{:.2f}'.format(self.Recon.surf_sphere_fit_resid_rms)+"$\pm$"+'{:.2f}'.format(np.abs(self.Recon.surf_sphere_fit_resid_rms_unc))+" $\mu$m RMS")
        self.SurfDiff_CenterMark.set_offsets([self.Data.x0_WF[i]*self.Data.conj_mag[i],self.Data.y0_WF[i]*self.Data.conj_mag[i]])
        
        self.EigenSurfImg.set_data(self.Recon.surf_EigenFuncs_fit_z)
        self.EigenSurfImg.set_clim(np.min(self.Recon.surf_EigenFuncs_fit_z),np.max(self.Recon.surf_EigenFuncs_fit_z))
        self.EigenSurf_CenterMark.set_offsets([self.Data.x0_WF[i]*self.Data.conj_mag[i],self.Data.y0_WF[i]*self.Data.conj_mag[i]])
        
        self.EigenSurfDiffImg.set_data(self.Recon.surf_EigenFuncs_fit_z_resid)
        self.EigenSurfDiffImg.set_clim(np.min(self.Recon.surf_EigenFuncs_fit_z_resid),np.max(self.Recon.surf_EigenFuncs_fit_z_resid))
        self.EigenSurfDiffAx.set_title("Eigenfunctions fit residual "+'{:.2f}'.format(self.Recon.surf_EigenFuncs_fit_z_resid_rms)+"$\pm$"+'{:.2f}'.format(np.abs(self.Recon.surf_sphere_fit_resid_rms_unc))+" $\mu$m RMS")
        self.EigenSurfDiff_CenterMark.set_offsets([self.Data.x0_WF[i]*self.Data.conj_mag[i],self.Data.y0_WF[i]*self.Data.conj_mag[i]])


    def plot_EigenFuncs_AnBn(self,fig,AnBns=None):
        ''' Plots the Eigenfunctions coefficients of specific eigenfunctions.
            AnBns sould be an iterable for pairs, each in the form (row,column) of the selected eigenfunction coefficient '''
        if AnBns == None:
            return
        '''
        if rows == None or rows > 2*self.Recon.EigenOrder_theta:
            rows = 2*self.Recon.EigenOrder_theta
        if columns == None or columns > self.Recon.EigenOrder_r+1:
            columns = self.Recon.EigenOrder_r+1
        '''
        k = 1
        for AnBn in AnBns:
            i,j = AnBn[0],AnBn[1]
            if (i > 2*self.Recon.EigenOrder_theta) or (j > self.Recon.EigenOrder_r+1):
                print("plot_EigenFuncs_AnBn: eigenfunction ("+str(i)+","+str(j)+") was not calculated, skipping this coefficient.")
                k+=1
                continue
            if k == 1:
                first_ax = fig.add_subplot(len(AnBns),1,1)
                first_ax.tick_params(axis='x', which='both', bottom=True, top=False, labelbottom=False, labeltop=False)
                first_ax_sec = first_ax.secondary_xaxis('top', functions=(self.Data.time_to_frame, self.Data.frame_to_time))
                first_ax.format_coord = cursor_format(first_ax, first_ax_sec)
                first_ax_sec.tick_params(axis='x', which='both', bottom=False, top=True, labelbottom=False, labeltop=True)
                first_ax_sec.set_xlabel("Frame #",fontsize=self.fontsize)
            else:
                current_ax = fig.add_subplot(len(AnBns),1,k,sharex = first_ax)
                if k < len(AnBns):
                    current_ax.tick_params(axis='x', which='both', bottom=True, top=False, labelbottom=False, labeltop=False)
            AnBn_array = np.full(self.Data.frame_count,np.nan)
            for l in range(self.Data.frame_count):
                try:
                    AnBn_array[l] = self.Data.Surf_EigenFuncs_fit_AnBn[l][i,j]
                except:
                    pass
            plt.plot(self.Data.time,AnBn_array)
            if i%2 == 0:
                plt.ylabel("$a_{%s%s}$\n[$\mu$m]" % (i,j),fontsize=self.fontsize)
            else:
                plt.ylabel("$b_{%s%s}$\n[$\mu$m]" % (i,j),fontsize=self.fontsize)
            k+=1
        plt.xlabel("Time [s]",fontsize=self.fontsize)
                

    def plot_EigenFuncs_full(self,i,rows=None,columns=None,cmap="jet"):
        fig = plt.figure(layout="tight")
        if rows == None or rows > 2*self.Recon.EigenOrder_theta:
            rows = 2*self.Recon.EigenOrder_theta
        if columns == None or columns > self.Recon.EigenOrder_r+1:
            columns = self.Recon.EigenOrder_r+1
        k = 1
        for i in range(rows):
            for j in range(columns):
                ax = fig.add_subplot(rows,columns,k)
                ax.text(-1.0,0.85,"$R_{%s%s}\Theta_{%s}$" % (i,j,i),fontsize=10)
                img = plt.imshow(self.Recon.EigenFuncs_full[i][j],origin='lower',extent=[-1,1,-1,1],cmap = cmap)
                cbar = Misc.add_colorbar(img)
                
                pupilCirc = plt.Circle((self.Data.x0_WF[i]*self.Data.conj_mag[i]/(self.Recon.d0/2),
                                             self.Data.y0_WF[i]*self.Data.conj_mag[i]/(self.Recon.d0/2)),
                                            self.Recon.d/self.Recon.d0,
                                            fill=False,edgecolor="black",linestyle='--',linewidth=1.5)
                plt.gca().add_artist(pupilCirc)
                k+=1

    
    def plot_time_evolution(self,fig,i):
        self.TimeEvolAx1 = fig.add_subplot(411)
        self.TimeEvolAx1.plot(self.Data.time,self.Data.AccInt_res,marker='.',alpha=0.5,linestyle='--',color='blue',
                                label="Internal IMU ("+"{:.3f}".format(self.Data.AccInt_res[i])+" g)")
        self.TimeEvolAx1.plot(self.Data.time,self.Data.AccExt_res_filt,marker='.',alpha=0.5,linestyle='--',color='green',
                                label="External IMU ("+"{:.3f}".format(self.Data.AccExt_res_filt[i])+" g)")
        self.TimeEvolTimeline1 = self.TimeEvolAx1.axvline(x=self.Data.time[i],color="black",linestyle='-.',
                                 label="Timeline (Frame "+str(i)+", Time "+"{:.3f}".format(self.Data.time[i])+" sec)")
        self.TimeEvolAx1.set_ylabel("Resultant proper acceleration [g]",fontsize=self.fontsize)
        self.TimeEvolAx1.set_xlim(np.min(self.Data.time),np.max(self.Data.time))
        self.TimeEvolAx1.set_ylim(-0.1,2.2)
        self.TimeEvolAx1_legend = self.TimeEvolAx1.legend(loc='upper right')
        self.TimeEvolAx1.tick_params(axis='x', which='both', bottom=True, top=False, labelbottom=False, labeltop=False)
        self.TimeEvolAx1_sec = self.TimeEvolAx1.secondary_xaxis('top', functions=(self.Data.time_to_frame, self.Data.frame_to_time))
        self.TimeEvolAx1_sec.xaxis.set_major_locator(MaxNLocator(integer=True))
        self.TimeEvolAx1_sec.set_xlim(0,self.Data.frame_count-1)
        self.TimeEvolAx1.format_coord = cursor_format(self.TimeEvolAx1, self.TimeEvolAx1_sec)
        self.TimeEvolAx1_sec.set_xlabel("Frame #",fontsize=self.fontsize)
        self.TimeEvolAx1_sec.tick_params(axis='x', which='both', bottom=False, top=True, labelbottom=False, labeltop=True)
        
        self.TimeEvolAx2 = fig.add_subplot(412, sharex = self.TimeEvolAx1)
        self.TimeEvolPlot2, = self.TimeEvolAx2.plot(self.Data.time,self.Data.SurfRoC,color='blue',linestyle='--',marker='.',label="Sphere fit RoC")
        self.TimeEvolAx2.set_xlim(np.min(self.Data.time),np.max(self.Data.time))
        try:
            self.TimeEvolAx2.set_ylim(0.98*np.nanmin(self.Data.SurfRoC),1.02*np.nanmax(self.Data.SurfRoC))
        except:
            pass
        self.TimeEvolAx2.set_ylabel("Sphere fit RoC [mm]",fontsize=self.fontsize)
        self.TimeEvolTimeline2 = self.TimeEvolAx2.axvline(x=self.Data.time[i],color="black",linestyle='-.',
                                 label="Timeline (Frame "+str(i)+", Time "+"{:.3f}".format(self.Data.time[i])+" sec)")
        self.TimeEvolAx2.tick_params(axis='x', which='both', bottom=True, top=False, labelbottom=False, labeltop=False)
        self.TimeEvolAx2.set_xlim(np.min(self.Data.time),np.max(self.Data.time))
        self.TimeEvolAx2_sec = self.TimeEvolAx2.secondary_xaxis('top', functions=(self.Data.time_to_frame, self.Data.frame_to_time))
        self.TimeEvolAx2_sec.tick_params(axis='x', which='both', bottom=False, top=True, labelbottom=False, labeltop=False)
        self.TimeEvolAx2_sec.xaxis.set_major_locator(MaxNLocator(integer=True))
        self.TimeEvolAx2_sec.set_xlim(0,self.Data.frame_count-1)
        self.TimeEvolAx2.format_coord = cursor_format(self.TimeEvolAx2, self.TimeEvolAx2_sec)
        
        self.TimeEvolAx3 = fig.add_subplot(413, sharex = self.TimeEvolAx1)
        self.TimeEvolPlot3, = self.TimeEvolAx3.plot(self.Data.time,self.Data.SurfErrRMS,color='red',linestyle='--',marker='.',label="Surface irregularity RMS")
        try:
            self.TimeEvolAx3.set_ylim(0.98*np.nanmin(self.Data.SurfErrRMS),1.02*np.nanmax(self.Data.SurfErrRMS))
        except:
            pass
        #self.TimeEvolAx3.set_xlabel("Time [s]",fontsize=self.fontsize)
        self.TimeEvolAx3.set_ylabel("Sphere fit residual RMS [$\mu$m]",fontsize=self.fontsize)
        self.TimeEvolAx3.tick_params(axis='x', which='both', bottom=True, top=False, labelbottom=False, labeltop=False)
        self.TimeEvolTimeline3 = self.TimeEvolAx3.axvline(x=self.Data.time[i],color="black",linestyle='-.',
                                 label="Timeline (Frame "+str(i)+", Time "+"{:.3f}".format(self.Data.time[i])+" sec)")
        self.TimeEvolAx3_sec = self.TimeEvolAx3.secondary_xaxis('top', functions=(self.Data.time_to_frame, self.Data.frame_to_time))
        self.TimeEvolAx3_sec.set_xlim(0,self.Data.frame_count-1)
        self.TimeEvolAx3_sec.tick_params(axis='x', which='both', bottom=False, top=True, labelbottom=False, labeltop=False)
        self.TimeEvolAx3_sec.xaxis.set_major_locator(MaxNLocator(integer=True))
        self.TimeEvolAx3.format_coord = cursor_format(self.TimeEvolAx3, self.TimeEvolAx3_sec)
        
        self.TimeEvolAx4 = fig.add_subplot(414, sharex = self.TimeEvolAx1)
        self.TimeEvolAx4.plot(self.Data.time,self.Data.PumpAction,label="Pump action (0 - Standby, 1- Running)")
        self.TimeEvolAx4.plot(self.Data.time,self.Data.PumpDirection,label="Pump direction (0 - Backward, 1 - Forward)")
        #self.TimeEvolAx4.plot(self.Data.time,self.Data.PumpMode,label="Pump mode (0 - Manual, 1 - Auto)")
        #self.TimeEvolAx4.plot(self.Data.time,self.Data.SyncButtonLabView,label="User button")
        self.TimeEvolTimeline4 = self.TimeEvolAx4.axvline(x=self.Data.time[i],color="black",linestyle='-.',
                                 label="Timeline (Frame "+str(i)+", Time "+"{:.3f}".format(self.Data.time[i])+" sec)")
        self.TimeEvolAx4.set_ylabel("Pump operations [a.u.]",fontsize=self.fontsize)
        self.TimeEvolAx4.set_xlabel("Time [s]",fontsize=self.fontsize)
        self.TimeEvolAx4.legend()
        self.TimeEvolAx4.tick_params(axis='x', which='both', bottom=True, top=False, labelbottom=True, labeltop=False)
        self.TimeEvolAx4_sec = self.TimeEvolAx4.secondary_xaxis('top', functions=(self.Data.time_to_frame, self.Data.frame_to_time))
        self.TimeEvolAx4_sec.xaxis.set_major_locator(MaxNLocator(integer=True))
        self.TimeEvolAx4_sec.set_xlim(0,self.Data.frame_count-1)
        self.TimeEvolAx4.format_coord = cursor_format(self.TimeEvolAx4, self.TimeEvolAx4_sec)
        self.TimeEvolAx4_sec.tick_params(axis='x', which='both', bottom=False, top=True, labelbottom=False, labeltop=False)

    
    def update_time_evolution_plot(self,i,autoscale=False):
        self.TimeEvolPlot2.set_ydata(self.Data.SurfRoC)
        self.TimeEvolPlot3.set_ydata(self.Data.SurfErrRMS)
        if autoscale:
            try:
                self.TimeEvolAx2.set_ylim(0.98*np.nanmin(self.Data.SurfRoC),1.02*np.nanmax(self.Data.SurfRoC))
                self.TimeEvolAx3.set_ylim(0.98*np.nanmin(self.Data.SurfErrRMS),1.02*np.nanmax(self.Data.SurfErrRMS))
            except:
                pass
        self.TimeEvolTimeline1.set_xdata(x=self.Data.time[i])    
        self.TimeEvolTimeline2.set_xdata(x=self.Data.time[i])
        self.TimeEvolTimeline3.set_xdata(x=self.Data.time[i])
        self.TimeEvolTimeline4.set_xdata(x=self.Data.time[i])
        self.TimeEvolAx1_legend.get_texts()[0].set_text("Internal IMU ("+"{:.3f}".format(self.Data.AccInt_res[i])+" g)")
        self.TimeEvolAx1_legend.get_texts()[1].set_text("External IMU ("+"{:.3f}".format(self.Data.AccExt_res_filt[i])+" g)")
        self.TimeEvolAx1_legend.get_texts()[2].set_text("Timeline (Frame "+str(i)+", Time "+"{:.3f}".format(self.Data.time[i])+" sec)")   
        
        
    def update_all_plots(self,i,autoscale=False):
        if hasattr(self,'EnvAx1'):
            self.update_Env_plot(i)
        self.update_WFplot(i)
        self.update_Zernikes(i)
        self.update_WFE_plot(i)
        self.update_PSF_plot()
        self.update_MTF_plot()
        self.update_recon_surface(i)
        if hasattr(self,'TimeEvolAx1'):
            self.update_time_evolution_plot(i,autoscale=autoscale)
        
        
    def plot_invalid(self,i):
        ''' Shows / updates the plots in the case of an invalid or failed frame '''
        self.update_WFplot(i)
        if hasattr(self,'EnvAx1'):
            self.update_Env_plot(i)
        if hasattr(self,'TimeEvolAx1'):
            self.update_time_evolution_plot(i)
        self.WFAx.set_title("Measured wavefront (SH plane) "+self.Data.time_RTC[i][:8]+"\n"
                                 #+"RoC N/A"
                                 +"f$_{MUT}$ N/A"
                                 +", d$_{MUT}$ N/A"
                                 +" mm")
        for j in range(len(self.ZerPlot)):
            self.ZerPlot[j].set_height(0)
        self.ZerSubplot.set_title("Zernike decomposition")
        self.ZerSubplot.set_ylim(-1,1)
        self.ZerPlotCenterTextBox.set_visible(True)

        self.ZerSubplot.set_title("Zernike decomposition\n$R^2$ = N/A, $R_{adj}^2$ = N/A")
        self.WFEImg.set_data(np.zeros_like(self.Recon.WFE.data))
        self.WFEAx.set_title("Wavefront aberration (MUT plane)\nRMS N/A, P-V N/A")
        if self.Recon.rigorous_response_analysis:
            self.PSFImg.set_data(np.zeros_like(self.Recon.PSF.data))
            self.AiryCirc.set_radius(0)
            self.MTFImg.set_data(np.zeros_like(self.Recon.MTF.data))
            self.MTFLimitContour.set_linewidth(0)
        self.Surf3Dplot.remove()
        self.RefSurf3D.remove()
        self.Surf3Dplot = self.Surf3DAx.plot_surface(self.Recon.x, self.Recon.y, np.zeros_like(self.Recon.z_surf), rasterized=True, alpha=0.0, label="Surface")
        self.RefSurf3D = self.Surf3DAx.plot_wireframe(self.Recon.x, self.Recon.y, np.zeros_like(self.Recon.surf_sphere_fit_z), alpha=0.0, label="Sphere fit\nRoC = N/A")
        self.Surf3DAxLegend.get_texts()[1].set_text("Sphere fit\nRoC = N/A")
        
        self.SurfImg.set_data(np.zeros_like(self.Recon.z_surf.data))
        
        self.SurfDiffImg.set_data(np.zeros_like(self.Recon.surf_sphere_fit_resid.data))
        self.SurfDiffAx.set_title("Sphere fit residual (N/A)")
        
        self.EigenSurfImg.set_data(np.zeros_like(self.Recon.surf_EigenFuncs_fit_z.data))
        
        self.EigenSurfDiffImg.set_data(np.zeros_like(self.Recon.surf_EigenFuncs_fit_z_resid.data))
        self.EigenSurfDiffAx.set_title("Eigenfunctions fit residual (N/A)")